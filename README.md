# SusySkim1LInclusive

This is where the SusySkim1LInclusive code lives and includes selectors for the 1L search.

See [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework)
for more details on the SusySkimAna code that our package relies on.

Currently based on `AnalysisBase,25.2.7`

### Overview
1. [Setup Instructions](#Setup-Instructions)
2. [Running Locally](#Running-Locally)
3. [Large-Scale Productions on the Grid](#Running-On-the-Grid)
4. [Remarks to Continous Integration](#Remarks-to-Continous-Integration)

## Setup Instructions

Remark: The tool ```acm``` (ATLAS compilation/package management) provides some useful wrappers for setting up the AnalysisRelease, compiling the code, etc. to make life a bit easier and is used in the following instructions. It is documented [here](https://gitlab.cern.ch/atlas-sit/acm/blob/master/README.md) and available after calling ```setupATLAS```.

Another remark: `setupATLAS` now sets up a python3 environment as default it recommended to manually restrict to python2 via `setupATLAS -2`.

### First Time Setup
Clone (this branch of) repository recursively with all submodules

    git clone --recursive ssh://git@gitlab.cern.ch:7999/vtudorac/1lepton.git;

Setup the Analysis Release
    
    cd onelep
    setupATLAS
    mkdir build; cd build;
    acmSetup AnalysisBase,25.2.7


To compile just type

    acm compile

### Subsequent Setups
For any further setups using the same Analysis Release it is sufficient to execute
```
acmSetup
```
within the ```build``` directory. In case new packages are added it may be
required to run ```acm find_packages``` before compiling.


## Running Locally
Below some example commands to run the selectors locally can be found.

**Remarks:** in case you are processing
* AFII files (such as signal samples typically), add the `--isAf2` flag
* data files, the add `--isData` flag

the commands below.

### 1L Search
Currently the [OneLep Selector](source/SusySkim1LInclusive/Root/OneLep.cxx) implements the selection for the 1L search and can be run with
```
SusySkim1LInclusive ()
 run_xAODNtMaker -s /afs/cern.ch/work/v/vtudorac/public/1lepton/run/mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5855/ -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config

run_xAODNtMaker -s /afs/cern.ch/work/v/vtudorac/public/1lepton/run/mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855/ -writeTrees 1 -selector OneLep -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config


## Running On the Grid
To govern large-scale production on the grid group sets containing multiple samples can be defined (see [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisFramework#Large_group_productions)). These group sets can then be submitted, and later downloaded and merged in one go. This requires to additionally load the following

```
lsetup panda
lsetup pyami
lsetup "rucio -w"
```

and to set up a voms proxy. Note that mc16a, mc16d and mc16e samples have to be submitted/downloaded/merged individually. For illustration,the set of commands used to submit the slepton signals are given at the end of each sections below.

### Submitting to Grid
To submit a group set

```
run_xAODNtMaker -groupSet MYGROUPSET  -selector MYSELECTOR -tag MYTAG --submitToGrid
```
RUN2
run_xAODNtMaker -groupSet mc20a -includeFiles  wjets_Sh_2211.txt  -selector OneLep -tag RUN2 -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run2.config -nFilesPerJob 1 --submitToGrid

RUN3
run_xAODNtMaker -groupSet mc23a -includeFiles  wjets.txt  -selector OneLep -tag RUN2 -deepConfig ../source/SusySkim1LInclusive/data/config/SusySkim1LInclusive_Run3.config -nFilesPerJob 1 --submitToGrid

**Note:** It's recommended to specify a destination of the grid output via the `-destinationSite` flag,
otherwise the output will be stored on some scratch disk where it will deleted after 2 weeks or so.
Ideally, the destination is a local group disk or something similar where there is
no constraint on the lifetime and the ntuple production can be downloaded also at a later
stage again if needed.

**Note2:** Currently it seems to be needed to give the grid some hint how many files of a sample should be
processed per job. Hence, it beneficial to add `-nGBPerJob 15` to the submission command.


### Download to Local Disc
To download the files that have been produced on the grid do the following (the download of the skims is skipped here)

```
run_download --disableSkims -d DOWNLOADDIR -u USER -tag MYTAG -groupSet MYGROUPSET -deepConfig CONFIG
```
where `USER` refers to the rucio account name of the user who submitted the tasks.

**Hint:** Downloading might take some time, in particular when experimental
systematics have been processed as well. You can "parallelize" this step to some extend
by using `-includeFiles` flag and e.g. one job per file to your local batch system.


### Merge Samples
To merge the downloaded samples type
```
run_merge -d DOWNLOADDIR -u USER -tag MYTAG -groupSet MYGROUPSET -deepConfig CONFIG
```
This step will also calculate the sum of weights and adjust the `genWeight` branch to include it. As default it will normalize the trees in MC to 1/pb, which can be adjusted with the flag `-lumi`.

A common approach for ntuple production is to use the `-lumi` flag to scale the mc16a/d/e samples accordingly to the integrated luminosity they represent (obviously this is needed for MC only). The values for Run 2 are `0.262/0.318/0.420` (these correspond to the final Run 2 lumi released early 2023). This allows then to add the merged samples together (via the `hadd` command from ROOT) so that at the end there is only one final ntuple to deal with that contains events from all mc16a/d/e.

**Hint:** Similar to the download step above, merging ntuples can take quite some
time hence it may be beneficial to also "parallalize" the merging step via a batch
system (again via the `-includeFiles` flag). Merging is also quite i/o intensive
so it's best done on some fast disk (typically the local `\tmp` dir suits that
purpose well).


## Monitoring Grid Productions
Unfortunately, we don't submit our tasks and they finish completely automatically,
at least not all of them. For a certain fraction some interventions are required,
mostly re-trying tasks that ended in finished state, i.e. did not process all available
events due to some temporary problems on the grid site (e.g. input files not available, ...).

The (to my knowledge) only official tool to make these interventions is `pbook` which is available
on the command line when you set up panda via `lsetup panda`. But this tool is arguably
not very convenient to use, hence people have built wrappers around it. One useful package
that might come in handy is [pandamonium](https://github.com/dguest/pandamonium).
It's probably best installed (in a virtual python environment) via pip

```shell
pip install pandamonium
```

There are a couple of examples in the package's README, but the most basic call to
list all your grid tasks can be already quite helpful (use your account name):

```shell
pandamon user.<your user name>
```

This gives you a nice brief overview of the progress of all your tasks (by just
sending an http request to the BigPanda web page). It has
quite some functionality, have a look via `pandamon -h`. It becomes really useful
as you can restrict to finished tasks and pipe them into a resubmit command:

```shell
pandamon user.<your user name> -i finished -t | panda-resub-taskid
```

That can safe a lot of work. As of now, two of of these "pipeable" wrappers around `pbook`
are shipped this package: `panda-resub-taskid` and `panda-kill-taskid`. They also come
with some options (see again via `-h`) that are worth knowing.

Final remark: there are probably plenty of other ways and tools to get your ntuple produced
on the grid (which essentially extend the poor functionality of the basis tools), that's just
one of them. Always look out if other people are doing something smarter!

### Tips & Tricks to known Problems during Grid Productions
Please find below a some solutions to problems that can occur when running on the
grid. Don't hesitate to add your own tips & tricks that might help others!

#### Handling exhausted tasks
Exhausted tasks should be resubmitted with more files per job which can be
achieved via a flag of the resubmit executable of pandamonium:

```shell
panda-resub-taskid -n X
```

## Remarks to Continous Integration
As usual, a set of instructions is defined in the [CI file](.gitlab-ci.yml) to build the project, run the selectors and validate their output after changes to the code and pushing them to the repository. To avoid broken pipelines, please find some remarks below to fix common problems.

### Updating the Analysis Release
After moving to a more recent Analysis Release it needs to be updated in several places:
* update the associated variable at the top of [.gitlab-ci.yml](.gitlab-ci.yml)
* update the name of the base image in [Dockerfile](Dockerfile)
* at the top of this README to keep the documentation up to date ;).



