#!/usr/bin/env python

import os, sys, argparse, json, math

import ROOT

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Read-in cutflow from file and compare with nominal one')
    parser.add_argument('-i', '--input', required=True, help="File to read cutflow from.")
    parser.add_argument('-n', '--nominal', required=True, help="Json file containing nominal cutflow to compare against.")
    parser.add_argument('--tolerance', type=int, default=5, help="allowed difference to nominal cutflow in [%]")
    args = parser.parse_args()
    
    if not os.path.exists(args.nominal):
        sys.exit("Json file {} with nominal cutflow does not exist!".format(args.nominal))
    
    with open(args.nominal) as w:
        info = json.load(w)
        labels = info["labels"]
        cf_nom = info["cutflow"]
        
        # weighted cutflow does not include normalization to SoW, thus its numbers
        # are a bit odd, so let's use the raw number of events for now
        t = ROOT.TFile(args.input)
        cf = t.Get("cutFlow___unweighted")
        if not cf:
            t.Close()
            sys.exit("Cannot retrieve cutflow from file {}!".format(args.input))
        
        print("Comparing unweighted cutflow with nominal values ...")
        for label in labels:
            # check if cut is present in the retrieved cutflow
            if label not in [str(l) for l in cf.GetXaxis().GetLabels()]:
                t.Close()
                sys.exit("Cutflow does not have cut {}!".format(label))
            # check if cut value agrees with nominal within tolerance
            n, n_nom = cf.GetBinContent(cf.GetXaxis().FindBin(label)), cf_nom[label]
            print("Cut: {:30s} : {:>7.1f} [nominal: {:>7.1f}]".format(label, n, n_nom))
            if (math.fabs(n - n_nom) / n_nom)  > args.tolerance/100.:
                t.Close()
                sys.exit("    ==> differs by more than {}% from nominal!".format(args.tolerance))
        
        print("Cutflow agrees with nominal within {}%!".format(args.tolerance))
        t.Close()
    
