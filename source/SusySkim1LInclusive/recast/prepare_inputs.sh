#!/bin/bash

########################
# Create xsecs file
########################

echo 'Creating xsecs file'

TEMP_XSEC=${1}
TEMP_DSID=${2}
touch xsec.txt
echo "dataset_number/I:physics_short/C:crossSection/D:genFiltEff/D:kFactor/D:relUncertUP/D:relUncertDOWN/D:generator_name/C:etag/C"  >> xsec.txt
echo "$TEMP_DSID hans $TEMP_XSEC 1.0000E+00 1.0 0.194200 0.0 hans helm" >> xsec.txt

########################
# Create deep config
########################

echo 'Building deep config file'

TEMP_PRWFILE=${3}
echo $TEMP_PRWFILE
TEMP_CAMPAIGN_UPPER=$(echo "${4}" | tr '[:lower:]' '[:upper:]')
if [ "$TEMP_CAMPAIGN_UPPER" == "MC16D" ]; then
    TEMP_CAMPAIGN_UPPER="MC16CD"
fi
echo "CrossSection : $PWD/xsec.txt" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/SusySkim1LInclusive_Recast.config
echo "PRW$TEMP_CAMPAIGN_UPPER : $TEMP_PRWFILE" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/SusySkim1LInclusive_Recast.config

########################
# Getting the sample name to prepare for download
########################

echo 'Get the sample name'
TEMP_INPUT=${5}
TEMP_SAMPLE=${TEMP_INPUT##*/}
TEMP_SAMPLE=${TEMP_SAMPLE%.txt}
mkdir $TEMP_SAMPLE

########################
# Building the config for merging later on
########################

echo 'Building merge config file for later'

if [[ "${6}"=="af2" ]]; then
    echo 'Got AF2 for merging later'
    TEMP_ISAF2="1"
else
    echo 'Got FS for merging later'
    TEMP_ISAF2="0"
fi

TEMP_PROCESS=${7}

echo "# NAME     : $TEMP_PROCESS" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/RECAST/recast.txt
echo "# DATA     : 0" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/RECAST/recast.txt
echo "# AF2      : $TEMP_ISAF2" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/RECAST/recast.txt
echo "# PRIORITY : 0" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/RECAST/recast.txt
echo "mc16_13TeV.${TEMP_DSID}.aprocess.deriv.DAOD_SUSY5.atag" | sudo tee -a $SUSYSkim1L_DIR/data/SusySkim1LInclusive/RECAST/recast.txt
