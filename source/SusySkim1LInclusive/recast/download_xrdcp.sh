#!/bin/bash

########################
# Downloading files
########################

echo 'Downloading input files'

while read FILE
do
    echo "$FILE"
    xrdcp $FILE $TEMP_SAMPLE
done < $TEMP_INPUT
