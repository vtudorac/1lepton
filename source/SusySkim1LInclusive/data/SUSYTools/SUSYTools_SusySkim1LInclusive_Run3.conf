##################################################
# SUSYTools configuration file
##################################################
Slices.Ele: 1
Slices.Pho: 1
Slices.Mu: 1
Slices.Tau: 1
Slices.Jet: 1
Slices.BJet: 1
Slices.FJet: 1
Slices.TJet: 1
Slices.MET: 1
#
IsRun3: true
#

EleBaseline.Pt: 4500
EleBaseline.Eta: 2.47
EleBaseline.Id: LooseAndBLayerLLH
EleBaseline.CrackVeto: false
EleBaseline.z0: 0.5
#
Ele.Et: 7000.
Ele.Eta: 2.47
Ele.CrackVeto: false

Ele.Iso: Tight_VarRad      #previously FCLoose
Ele.IsoHighPtThresh: 75
Ele.IsoHighPt: Tight_VarRad      #previously FCHighPtCaloOnly
#
Ele.Id: TightLLH
Ele.d0sig: 5.
Ele.z0: 0.5
# ChargeIDSelector WP
Ele.CFT: None
#
MuonBaseline.Pt: 3000
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 1 # Medium
MuonBaseline.z0: 0.5
#
Muon.Pt: 6000.
Muon.Eta: 2.5
Muon.Id: 1 # Medium
Muon.Iso: PflowLoose_VarRad # PLVLoose    # previously FCLoose -> Loose_VarRad
Muon.IsoHighPtThresh: 75
Muon.IsoHighPt: PflowTight_VarRad  # previously FCTightTrackOnly -> TightTrackOnly_VarRad
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.4
#
PhotonBaseline.Pt: 25000.
PhotonBaseline.Eta: 2.37
PhotonBaseline.Id: Tight
#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.50
Tau.Id: Medium
#Tau.DoTruthMatching: false
#
Jet.Pt: 30000.
Jet.Eta: 2.8
Jet.InputType: 9 # PFlow
#Jet.JVT_WP: Tight
#Jet.JvtWP: Tight
Jet.JvtWP: FixedEffPt
Jet.JvtPtMax: 60e3
#Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_FullJER.config
Jet.UncertConfig: rel22/Winter2023_PreRec/R4_SR_Scenario1_SimpleJER.config
#Jet.UncertPDsmearing: true
Jet.UncertPDsmearing: false
#
FwdJet.doJVT: false
FwdJet.JvtEtaMin: 2.5
FwdJet.JvtPtMax: 120e3

# FatJets
Jet.LargeRuncConfig: None
Jet.LargeRcollection: AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets #AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
Jet.JESConfigFat: JES_MC16recommendation_R10_UFO_CSSK_SoftDrop_JMS_01April2020.config
Jet.JESConfigFatData: JES_MC16recommendation_R10_UFO_CSSK_SoftDrop_JMS_01April2020.config



Jet.WtaggerConfig:SmoothedContainedWTagger_AntiKt10UFOCSSKSoftDrop_FixedSignalEfficiency80_20220221.dat
Jet.ZtaggerConfig: SmoothedContainedZTagger_AntiKt10UFOCSSKSoftDrop_FixedSignalEfficiency80_20220221.dat
Jet.ToptaggerConfig: None #DNNTagger_AntiKt10UFOSD_TopInclusive80_Oct30.dat
Jet.WZTaggerCalibArea: SmoothedWZTaggers/Rel21/February2022/
#Jet.LargeRuncVars: pT,Tau21WTA,Split12
#
BadJet.Cut: LooseBad
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
Btag.Tagger: DL1dv01 #DL1r
Btag.WP: FixedCutBEff_85 #Continuous
Btag.CalibPath: xAODBTaggingEfficiency/13p6TeV/2023-22-13TeV-MC21-CDI-2023-09-13_v1.root
Btag.MinPt: 20000.
BtagTrkJet.MinPt: 10000.

BtagTrkJet.enable: false
BtagTrkJet.Tagger:DL1r #DL1dv01 
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets

#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: false
OR.ElBjet: false
OR.MuBjet: false
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
#add fatjets to OR
OR.DoFatJets: false
#OR.InputLabel: selected
#
SigLep.RequireIso: true
#SigLepPh.IsoCloseByOR: false
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight
MET.RemoveOverlappingCaloTaggedMuons: true
MET.DoRemoveMuonJets: true
MET.UseGhostMuons: false
MET.DoMuonEloss: false

Truth.UseTRUTH3: true

Trigger.UpstreamMatching: false
Trigger.MatchingPrefix: TrigMatch_

#
Ele.TriggerSFStringSingle: 2022_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI

#
# actual Mu files have to be set in SUSYTools

PRW.ActualMu2022File: GoodRunsLists/data22_13p6TeV/20230207/purw.actualMu.2022.root
PRW.ActualMu2023File: GoodRunsLists/data23_13p6TeV/20230828/purw.actualMu.2023.root

#
PRW.autoconfigPRWRtags:mc23a:r14622_r14932,mc23c:r14799_r14908,mc23d:r15224

StrictConfigCheck: true
