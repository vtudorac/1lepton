##############################################
#                                            
#   SusySkimAna Continuous Integration       
#   Heavy reference from MBJ_Analysis: https://gitlab.cern.ch/MultiBJets/MBJ_Analysis
#      
##############################################

stages:
  - build 
  - package
  - run

variables:
  GIT_STRATEGY: fetch
  GIT_SUBMODULE_STRATEGY: none
  AB_FIXED_RELEASE: 21.2.125
  AB_LATEST_RELEASE: latest
  SRC_DIR_ABS: "${CI_PROJECT_DIR}/../"
  BUILD_DIR_ABS: "${CI_PROJECT_DIR}/../../build/"
  TESTSAMPLE_HOME: "root://eosuser.cern.ch:/eos/user/s/susyskimana/CI/DAOD/"

before_script:
  # Install ssh-agent if not already installed, it is required by Docker.
  # (change apt-get to yum if you use a CentOS-based image)
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

  # Run ssh-agent (inside the build environment)
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY"
  - echo "$SSH_PRIVATE_KEY" > tmpKey
  - chmod 600 tmpKey
  - ssh-add tmpKey
  - mkdir -p ~/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  - echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
  - klist
  - echo -e "Host svn.cern.ch lxplus.cern.ch\n\tUser ${CERN_USER}\n\tStrictHostKeyChecking no\n\tGSSAPIAuthentication yes\n\tGSSAPIDelegateCredentials yes\n\tProtocol 2\n\tForwardX11 no\n\tIdentityFile ~/.ssh/id_rsa" >> ~/.ssh/config 
  - more ~/.ssh/config
  - set +e
  - export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  - source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c centos7 --quiet
  - pwd
  - ls
  - echo "Project Directory    ${CI_PROJECT_DIR}/../../"
  - echo "Source Directory     ${SRC_DIR_ABS}"
  - echo "Build Directory      ${BUILD_DIR_ABS}"

################################
#  Build
################################
.build_template:
  stage: build
  variables:
    GIT_STRATEGY: fetch
    GIT_SUBMODULE_STRATEGY: none
  tags:
    - cvmfs
  script:
    - bash
    - cd ../
    - pwd;ls
    - echo "Make sure cvmfs is visible"
    - ls /cvmfs/atlas.cern.ch/
    
    # Set up the AB release
    - source /home/atlas/release_setup.sh
    # Checkout packages and generate top-level CMakeLists.txt. Note that AB release specified here must be the same as the one chosen here
    - source SusySkimMaker/scripts/setupRelease.sh --skip-kinit 
    # Checkout selector: SusySkimValidation 
    - echo "Cloning SusySkimValidation..."
#    - git clone -b "dev-merge-interfacechange" --single-branch ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimValidation.git
    - git clone ssh://git@gitlab.cern.ch:7999/SusySkimAna/SusySkimValidation.git
    - echo "Copy the ST config from the install area of the AB release & Update the actualMu config for 2018."
    - cat ${AnalysisBase_DIR}/src/PhysicsAnalysis/SUSYPhys/SUSYTools/data/SUSYTools_Default.conf
    - cp ${AnalysisBase_DIR}/src/PhysicsAnalysis/SUSYPhys/SUSYTools/data/SUSYTools_Default.conf ${SRC_DIR_ABS}/SusySkimValidation/data/SUSYTools/SUSYTools_SusySkimValidation_Default.conf

    # Build
    - cd ../build/
    - cmake ${SRC_DIR_ABS}
    - make -j8
    - echo "${AnalysisBase_PLATFORM}/setup.sh"
    - ls "${AnalysisBase_PLATFORM}/setup.sh"
    - source "${AnalysisBase_PLATFORM}/setup.sh"
    - if ! (type run_xAODNtMaker > /dev/null 2>&1); then echo "Compile failed. Exit."; exit 1; fi
    - echo "Make sure the env/paths are correctly set."; run_xAODNtMaker -h
    - echo "Generate RPM..."; cpack -G RPM
    - ls -lavh
    - mv WorkDir_*.rpm ${CI_PROJECT_DIR}/susyskim_ana.rpm
    - ls ${CI_PROJECT_DIR}
    - ls ${CI_PROJECT_DIR}/../
  artifacts:
    name: rpm
    paths:
      - susyskim_ana.rpm
      - Dockerfile
    expire_in: 1 day

################################
# build RPMs of the analysis code to install in docker image

compile:
  extends: .build_template
  image: atlas/analysisbase:$AB_FIXED_RELEASE

#compile_latest:
#  extends: .build_template
#  image: atlas/analysisbase:$AB_LATEST_RELEASE

################################
#  Image building
################################

# This is a package template that all package jobs use to package the RPMs.
.package_template:
  stage: package
  dependencies: [compile]
  variables:
    BUILD_ARG_1: CI_COMMIT_SHA=$CI_COMMIT_SHA
    BUILD_ARG_2: CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
    BUILD_ARG_3: CI_COMMIT_TAG=$CI_COMMIT_TAG
    BUILD_ARG_4: CI_JOB_URL=$CI_JOB_URL
    BUILD_ARG_5: CI_PROJECT_URL=$CI_PROJECT_URL
  tags:
    - docker-image-build
  script:
    - ignore

#####################################
# Push image to latest in docker-registry
# This is always run.
# If tagged, CI_COMMIT_REF_SLUG is the tag name with dashes

package_image:
  extends: .package_template
  stage: package
  variables:
    TO: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  dependencies: [compile]

################################
# Run  
################################
# Anything that wants to use the provided analysis image (with set up) should
# extend this template.  This specifies the built image, as well as the correct
# initialization to get MBJ_run.py and kinit access.
.analysis_image:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - ls -lavh /
    - ls -lavh /usr/WorkDir/
    - source /home/atlas/release_setup.sh
    - echo $SERVICE_PASS | kinit $CERN_USER

# This template is used by anything that wants to process a provided sample located in /eos/user.
.run_DAOD:
  extends: .analysis_image
  variables:
    NEVENTS: 1000
  stage: run
  script:
    - echo "CI_JOB_NAME=${CI_JOB_NAME}"
    - export submitDir="${CI_JOB_NAME}_vTest"
    - mkdir -p ${submitDir}
    - ls -lavh /eos/
    - echo "run_xAODNtMaker -d ${CI_JOB_NAME} -tag vTest -s ${INPUTDIR} -writeSkims 1 -writeTrees 1 -deepConfig SusySkimValidation_Rel21.config -selector Validation --useXROOTd -DAODKernelName ${DERIVNAME}KernelSkim -MaxEvents ${NEVENTS}"
    - run_xAODNtMaker -d ${CI_JOB_NAME} -tag vTest -s ${INPUTDIR} -writeSkims 1 -writeTrees 1 -deepConfig SusySkimValidation_Rel21.config -selector Validation --useXROOTd -DAODKernelName ${DERIVNAME}KernelSkim -MaxEvents ${NEVENTS}
    - ls -lavh
    - ls -lavh ${submitDir}
  artifacts:
    name: submitDir
    paths:
      - ${CI_JOB_NAME}_vTest/
    expire_in: 1 day

#############################
run_mc16e_ttbar_SUSY1_p3990:
  extends: .run_DAOD
  variables:
    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY1.e6337_s3126_r10724_p3990/
    DERIVNAME: SUSY1
  stage: run
############
run_mc16e_ttbar_SUSY2_p3990:
  extends: .run_DAOD
  variables:
    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY2.e6337_s3126_r10724_p3990/
    DERIVNAME: SUSY2
  stage: run
############
#run_mc16e_ttbar_SUSY3_p3712:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY3.e6337_s3126_r10724_p3712
#    DERIVNAME: SUSY3
#  stage: run
############
#run_mc16e_ttbar_SUSY4_p3652:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY4.e6337_s3126_r10724_p3652
#    DERIVNAME: SUSY4
#  stage: run
############
run_mc16e_ttbar_SUSY5_p3990:
  extends: .run_DAOD
  variables:
    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6337_s3126_r10724_p3990
    DERIVNAME: SUSY5
  stage: run
############
#run_mc16e_ttbar_SUSY9_p3613:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY9.e6337_s3126_r10724_p3613
#    DERIVNAME: SUSY9
#  stage: run
############
#run_mc16e_ttbar_SUSY10_p3759:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY10.e6337_s3126_r10724_p3759
#    DERIVNAME: SUSY10
#  stage: run
############
run_mc16e_ttbar_SUSY11_p3990:
  extends: .run_DAOD
  variables:
    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY11.e6337_s3126_r10724_p3990
    DERIVNAME: SUSY11
  stage: run
############
#run_mc16e_ttbar_SUSY16_p3652:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY16.e6337_s3126_r10724_p3652
#    DERIVNAME: SUSY16
#  stage: run
############
#run_mc16e_ttbar_SUSY17_p3652:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY17.e6337_s3126_r10724_p3652
#    DERIVNAME: SUSY17
#  stage: run
############
#run_mc16e_ttbar_SUSY18_p3840:
#  extends: .run_DAOD
#  variables:
#    INPUTDIR: ${TESTSAMPLE_HOME}/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY18.e6337_s3126_r10724_p3840
#    DERIVNAME: SUSY18
#  stage: run
#############################

