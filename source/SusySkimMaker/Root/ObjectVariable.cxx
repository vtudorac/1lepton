#include "SusySkimMaker/ObjectVariable.h"
#include "TString.h"


ObjectVariable::ObjectVariable() 
{
  setDefault(barcode,0);
}
// ------------------------------------------------------------------------------------- //
ObjectVariable::ObjectVariable(const ObjectVariable &rhs):
  TLorentzVector(rhs),
  triggerMap(rhs.triggerMap),
  truthTLV(rhs.truthTLV),
  barcode(rhs.barcode)
{

}
// ------------------------------------------------------------------------------------- //
ObjectVariable& ObjectVariable::operator=(const ObjectVariable &rhs)
{
  if (this != &rhs) {
    TLorentzVector::operator=(rhs);
    triggerMap   = rhs.triggerMap;
    truthTLV     = rhs.truthTLV;
    barcode      = rhs.barcode;
  }
  return *this;
}
// ------------------------------------------------------------------------------------- //
void ObjectVariable::setDefault( float& var, float def)
{
  //
  var = def;
}
// ------------------------------------------------------------------------------------- //
void ObjectVariable::setDefault( bool& var, bool def)
{
  var = def;
}
// ------------------------------------------------------------------------------------- //
void ObjectVariable::setDefault( int& var, int def)
{
  var = def;
}
// ------------------------------------------------------------------------------------- //
void ObjectVariable::setDefault( uint8_t& var, uint8_t def)
{
  var = def;
}
// ------------------------------------------------------------------------------------- //
bool ObjectVariable::trigMatchResult(const TString trigChain)
{

  auto it = this->triggerMap.find(trigChain);

  // Found the chain
  if( it != this->triggerMap.end() ){
    return it->second;
  }
  // Didn't find the chain
  else{
    return false;
  }

}
