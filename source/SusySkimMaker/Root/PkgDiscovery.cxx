#include <fstream>
#include <string>
#include "TSystem.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/BaseUser.h"
#include "TSystem.h"
#include "PathResolver/PathResolver.h"
/*

  Possible usage:
    1) You provide a selector, but no deep configuration file. The selector is used to determine 
       the package name, and the default configuration file for this package is used
    2) You provide a deep configuration file, but no selector. Since the selector is required to
       determine the package, the deep configuration file has to be specified in full, and this
       differs between R20.7 and R21. Here are examples
         - R20.7 : $ROOTCOREBIN/data/<packageName>/config/<nameOfDeepConfig.config>
         - R21   : <packageName>/<nameOfDeepConfig.config>
    3) Similar to (3), you provide a deep configuration file but no selector, but, you've used 
        the set_groupset_env.sh script to configure the env variables. In this case, the env variable 
        is detected, but it *required* to have the formatting as above, so we know which 
        package the configuration file resides in.

    4) You provide nothing at all. The default deep configuration from SusySkimMaker is used

   In *all* cases, it's highly discouraged to specify the full expanded path, rather you should 
   always specify relative to the $ROOTCOREBIN, or as indicated above in R21. If you do not, 
   then you will forsure have problems when running on any batch system

*/


// Static data members
static TString m_deepConfig;
static TString m_deepConfigFullPath;
static TString m_SUSYToolsConfig;
static TString m_selector;

static TString DEFAULT_PACKAGE = "SusySkimMaker";

//
StatusCode PkgDiscovery::Init(TString deepConfig,TString selector)
{

  // Default is to look for configuration files in this 
  // package, unless the user is running with their own 
  // analysis selector package
  TString packageName = DEFAULT_PACKAGE;
  int rc_release = atoi(gSystem->Getenv("ROOTCORE_RELEASE_SERIES"));
  std::string confFile;

  // Change package name according to user's selector
  BaseUser* AnalysisSelector = 0;
  if( !selector.IsWhitespace() ){
    AnalysisSelector = getAnalysisSelector(selector);
    if( AnalysisSelector ){
      packageName = AnalysisSelector->getPackageName();
    }
  }

  // ********************************************************** //
  std::cout<<"deepConfig.IsWhitespace() = "<<deepConfig.IsWhitespace()<<std::endl;
  //  std::cout<<"ROOTCORE_RELEASE_SERIES = "<<ROOTCORE_RELEASE_SERIES<<std::endl;
  if(AnalysisSelector)std::cout<<"AnalysisSelector = 1"<<std::endl;
  else std::cout<<"AnalysisSelector = 0"<<std::endl;
  // Option (1)
  if( AnalysisSelector && deepConfig.IsWhitespace() ){
    if(rc_release > 24)
      deepConfig = packageName + "_Rel24.config";
    else
      deepConfig = packageName + "_Default.config";
    
    MsgLog::INFO("PkgDiscovery::Init","No deep configuration file specified by the user, trying to use default %s", deepConfig.Data() );
  }

  // Option (2) requires no work at this point

  // Option (3)
  if( deepConfig.IsWhitespace() ){
    TString auto_field = TString( gSystem->Getenv( "GROUP_SET_DEEPCONFIG" ) );
    if( !auto_field.IsWhitespace() ){
      deepConfig = auto_field;
      MsgLog::INFO("PkgDiscovery::Init","Auto detected deep configuration file from an env variable: %s",deepConfig.Data() );
    }
  }

  // Option (4)
  if( !AnalysisSelector && deepConfig.IsWhitespace() ){
    if(rc_release > 24)
      deepConfig = DEFAULT_PACKAGE + "_Rel24.config";
    else
      deepConfig = DEFAULT_PACKAGE + "_Default.config";
    //
    MsgLog::INFO("PkgDiscovery::Init","User provided no selector or deep configuration file, using default %s",deepConfig.Data() ); 
  }

  // Should never happen, but added for protection incase the logic above gets changed!
  if( deepConfig.IsWhitespace() ){
    MsgLog::ERROR("PkgDiscovery::Init","User did not provide a deep configuration and could not extract the default. Impossible to configure running!");
    return StatusCode::FAILURE;
  } 

  // Compiling with CMAKE
  // Discovery information based upon 
  // most likely scenarios
  if(rc_release > 24){

  MsgLog::INFO("PkgDiscovery::Init","Detected a Release 24 package setup compiled within the CMAKE environment");

  // If the user provided a selector,
  // ensure that the deep configuration file is
  // formatted as discussed in Option (2)
  if( AnalysisSelector ){
    if( !deepConfig.Contains( TString::Format("%s/",packageName.Data() )) ){
      deepConfig = TString::Format("%s/%s",packageName.Data(),deepConfig.Data() );
    }
  } else {
    // if no selector given, first assume user already gave the full path
    confFile = PathResolverFindCalibFile( deepConfig.Data() );

    // otherwise try to add DEFAULT_PACKAGE in front of the path
    if( !std::ifstream (confFile).good() ) {
      deepConfig = TString::Format("%s/%s",DEFAULT_PACKAGE.Data(),deepConfig.Data() );
    }
  }

  // Search for file!
  confFile = PathResolverFindCalibFile( deepConfig.Data() );
  if( !std::ifstream (confFile).good() ) {
    MsgLog::ERROR("","Could not locate deep configuration file: %s",deepConfig.Data() );
    return StatusCode::FAILURE;
  }


// Compiling with ROOTCORE CMT
  }else{
  MsgLog::INFO("PkgDiscovery::Init","Detected a Release 20 package setup compiled within the ROOTCORE CMT environment");

  // Required to be specified with respect to the $ROOTCOREBIN env variable.
  // Note, users should *NOT* specify absolute paths, since this will
  // likely break on the worker nodes
  if( !deepConfig.Contains("$ROOTCOREBIN") && !deepConfig.Contains( gSystem->ExpandPathName("$ROOTCOREBIN")  )){
    deepConfig = TString::Format("$ROOTCOREBIN/../%s/data/config/%s",packageName.Data(),deepConfig.Data() );
  }

  // Search for file!
  confFile = gSystem->ExpandPathName( deepConfig.Data() );
  if( !std::ifstream (confFile).good() ) {
    MsgLog::ERROR("","Could not locate deep configuration file: %s",deepConfig.Data() );
    return StatusCode::FAILURE;
  }

 }

  // Save 
  m_deepConfig         = deepConfig;
  m_deepConfigFullPath = TString( confFile.c_str() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------- //
TString PkgDiscovery::getDeepConfigFullPath()
{
  return m_deepConfigFullPath;
}
// ------------------------------------------------------------------- //
TString PkgDiscovery::getDeepConfig()
{
  return m_deepConfig;
}



