#include "SusySkimMaker/ArgParser.h"

ArgParser::ArgParser() : m_help("-help")
{

}
// -------------------------------------------------------------------- //
bool ArgParser::parse(const char* inputFlag, const char* flag, const char* message)
{

  // User passed this inputFlag
  if (strcmp(inputFlag,flag) == 0   ){
    return true;
  }
 
  // Print out message
  if (strcmp(inputFlag,m_help.Data() ) == 0 ){
    printf("  => %-30s : %-50s \n",flag,message);
  }

  return false;


}
// -------------------------------------------------------------------- //

