#include "SusySkimMaker/MuonObject.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTruth/TruthParticle.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/TreeMaker.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/ConstAccessors.h"
#include "SusySkimMaker/Constants.h"
#include "SusySkimMaker/TrackObject.h"
#include "SusySkimMaker/Timer.h"
#include <sstream>

MuonObject::MuonObject() : m_muonSelectionTool(0), 
			   m_isData(false),
                           m_baseObject(0),
                           m_trackObject(0),
			   m_writeDetailedSkim(false),
			   m_isRun3(false)
{
  m_muonVectorMap.clear();
}
// ------------------------------------------------------------------------- //
StatusCode MuonObject::init(TreeMaker*& treeMaker, bool isData)
{

  const char* APP_NAME = "MuonObject";

  m_isData = isData;

  for( auto& sysName : treeMaker->getSysVector() ){
    MuonVector* mu  = new MuonVector();
    // Save muon vector into map
    m_muonVectorMap.insert( std::pair<TString,MuonVector*>(sysName,mu) );
    // Get tree created by createTrees
    TTree* sysTree = treeMaker->getTree("skim",sysName);
    // Don't write it out
    if(sysTree==NULL) continue;
    else{
      Info("MuonObject::init", "Adding a branch muons to skims: %s", sysTree->GetName() );
      std::map<TString,MuonVector*>::iterator muonItr = m_muonVectorMap.find(sysName);
      sysTree->Branch("muons",&muonItr->second);
    }      
  }

  //
  CHECK( init_tools() );

  // Return gracefully
  return StatusCode::SUCCESS;

}
// ------------------------------------------------------------------------- //
StatusCode MuonObject::init_tools()
{

  const char* APP_NAME = "MuonObject";

  // MuonSelectionTool
  m_muonSelectionTool = new CP::MuonSelectionTool("MuonObject_MuonSelectionTool");
  CHECK( m_muonSelectionTool->initialize() );
  CentralDB::retrieve(CentralDBFields::ISRUN3, m_isRun3);
  if( m_isRun3 ){
    CHECK( m_muonSelectionTool->setProperty("IsRun3Geo", true ) );
    }

  // Isolation WPs
  m_baseObject = new BaseObject();
  CHECK( m_baseObject->initialize("MuonWP") );

  // 
  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);
  CentralDB::retrieve(CentralDBFields::WRITEDETAILEDSKIM,m_writeDetailedSkim);

  return StatusCode::SUCCESS;
 
}
// ------------------------------------------------------------------------- //
void MuonObject::fillMuonContainer(xAOD::Muon* muon_xAOD,
				   const xAOD::VertexContainer* primVertex,
                                   const xAOD::EventInfo* eventInfo,
				   const std::map<TString,bool> trigMap,
				   const std::map<TString,float> recoSF,
                                   MCCorrContainer* trigEff, 
				   std::string sys_name)
{

  //
  Timer::Instance()->Start( "MuonObject::fillMuonContainer" );

  std::map<TString,MuonVector*>::iterator it = m_muonVectorMap.find(sys_name);

  if( it==m_muonVectorMap.end() ){
    std::cout << "<MuonObject::fillMuonContainer> ERROR Request to get muon for unknown systematic: " << sys_name << std::endl;
    return;
  }

  //
  MuonVariable* mu = new MuonVariable();

  const xAOD::TrackParticle* track   = muon_xAOD->primaryTrackParticle();
  const xAOD::TrackParticle* idtrack = muon_xAOD->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
  const xAOD::TrackParticle* metrack = muon_xAOD->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );

  // TLV
  mu->SetPtEtaPhiM( muon_xAOD->pt() * m_convertFromMeV,
		    muon_xAOD->eta(),
		    muon_xAOD->phi(),
		    muon_xAOD->m() * m_convertFromMeV );

  // Save ID track link
  if( m_trackObject ){
    mu->trkLink = m_trackObject->fillTrack(idtrack,primVertex,eventInfo,TrackVariable::TrackType::MUONLINK,sys_name);
  }

  // Charge
  mu->q = muon_xAOD->charge();

  // Impact parameters
  mu->d0     = TrackObject::getD0(track);
  mu->z0     = TrackObject::getZ0(track,primVertex);
  mu->d0Err  = TrackObject::getD0Err(track,eventInfo);
  mu->z0Err  = TrackObject::getZ0Err(track);
  mu->pTErr  = TrackObject::getPtErr(track);

  // Primary author of the muon
  mu->author = muon_xAOD->author();
  
  // Quality of muon, from MST
  mu->quality = getMuonQuality(muon_xAOD);

  // Muon LowPtWP: quality == 5 (but not a subset of the others)
  mu->lowPtWp = m_muonSelectionTool->passedLowPtEfficiencyCuts( *muon_xAOD );

  // SUSYTools defs
  mu->signal = cacc_signal(*muon_xAOD);
  mu->passOR = cacc_passOR(*muon_xAOD);
  mu->bad    = cacc_bad(*muon_xAOD);
  mu->cosmic = cacc_cosmic(*muon_xAOD);
  //mu->baseline_EWKcomb = cacc_baseline_EWKcomb.isAvailable( *muon_xAOD ) ? cacc_baseline_EWKcomb( *muon_xAOD ) : false;

  //
  // Isolation from selector tools
  //

  // Turn off PLVWPs if the file doesn't contain the PLV variables
  if(!cacc_promptLepVeto.isAvailable( *muon_xAOD ) && !m_baseObject->havePLVWPsRemoved)
    m_baseObject->disablePLVWPs(xAOD::Type::Muon);

  // Turn off PLIVWPs if the file doesn't contain the PLIV variables
  if(!cacc_promptLepImprovedVetoMuons.isAvailable( *muon_xAOD ) && !m_baseObject->havePLIVWPsRemoved)
    m_baseObject->disablePLIVWPs(xAOD::Type::Muon);

  // Calculate the LowPtPLV score if it can be and not yet
  // (See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PromptLeptonTaggerIFF)
  if(cacc_promptLepVeto.isAvailable( *muon_xAOD ) && !cacc_LowPtPLV.isAvailable( *muon_xAOD ))
    m_baseObject->isoLowPtPLVTool->augmentPLV(*muon_xAOD);

  const asg::AcceptData isIsoSel   = m_baseObject->isoTool->accept(*muon_xAOD);
  
  //Old WPs
  // Rel22 WPs
  //Conventional track+calo WPs are maintained for analyses with special needs 
  mu->IsoLoose_VarRad            = isIsoSel.getCutResult("Loose_VarRad");
  mu->IsoLoose_FixedRad          = isIsoSel.getCutResult("Loose_FixedRad");
  mu->IsoTight_VarRad            = isIsoSel.getCutResult("Tight_VarRad");
  mu->IsoTight_FixedRad          = isIsoSel.getCutResult("Tight_FixedRad");
  //Track only WPs are also recommended. 
  mu->IsoTightTrackOnly_VarRad   = isIsoSel.getCutResult("TightTrackOnly_VarRad");
  mu->IsoTightTrackOnly_FixedRad = isIsoSel.getCutResult("TightTrackOnly_FixedRad");
  mu->IsoHighPtTrackOnly         = isIsoSel.getCutResult("HighPtTrackOnly"); 
  // Pflow WPs are most recommended. They should cover most use cases 
  mu->IsoPflowLoose_VarRad       = isIsoSel.getCutResult("PflowLoose_VarRad");  
  mu->IsoPflowLoose_FixedRad     = isIsoSel.getCutResult("PflowLoose_FixedRad");
  mu->IsoPflowTight_VarRad       = isIsoSel.getCutResult("PflowTight_VarRad");  
  mu->IsoPflowTight_FixedRad     = isIsoSel.getCutResult("PflowTight_FixedRad");
  if(!m_baseObject->havePLVWPsRemoved){
    mu->IsoPLVLoose                = isIsoSel.getCutResult("PLVLoose");
    mu->IsoPLVTight                = isIsoSel.getCutResult("PLVTight");
  }
  if(!m_baseObject->havePLIVWPsRemoved){
    mu->IsoPLImprovedTight         = isIsoSel.getCutResult("PLImprovedTight");
    mu->IsoPLImprovedVeryTight     = isIsoSel.getCutResult("PLImprovedVeryTight");
  }
  
  
  // Isolation variables, coming from xAODs
  // ptcone
  float ptcone20 = 0.0;
  float ptcone30 = 0.0;
  float ptcone40 = 0.0;
  muon_xAOD->isolation(ptcone20,xAOD::Iso::ptcone20);
  muon_xAOD->isolation(ptcone30,xAOD::Iso::ptcone30);
  muon_xAOD->isolation(ptcone40,xAOD::Iso::ptcone40);
  mu->ptcone20     = ptcone20 * m_convertFromMeV; 
  mu->ptcone30     = ptcone30 * m_convertFromMeV;
  mu->ptcone40     = ptcone40 * m_convertFromMeV;

  // etcone
  float etcone30 = 0.0;
  muon_xAOD->isolation(etcone30,xAOD::Iso::etcone30);
  mu->etcone30     = etcone30 * m_convertFromMeV;

  // topoetcone
  float topoetcone20 =0.0;
  float topoetcone30 =0.0;
  float topoetcone40 =0.0;
  muon_xAOD->isolation(topoetcone20,xAOD::Iso::topoetcone20);
  muon_xAOD->isolation(topoetcone30,xAOD::Iso::topoetcone30);
  muon_xAOD->isolation(topoetcone40,xAOD::Iso::topoetcone40);
  mu->topoetcone20 = topoetcone20 * m_convertFromMeV; 
  mu->topoetcone30 = topoetcone30 * m_convertFromMeV;
  mu->topoetcone40 = topoetcone40 * m_convertFromMeV;

  // ptvarcone
  mu->ptvarcone20 = cacc_ptvarcone20.isAvailable(*muon_xAOD) ? 
    cacc_ptvarcone20(*muon_xAOD) * m_convertFromMeV: -1.0;
  mu->ptvarcone30 = cacc_ptvarcone30.isAvailable(*muon_xAOD) ? 
    cacc_ptvarcone30(*muon_xAOD) * m_convertFromMeV : -1.0;
  mu->ptvarcone40 = cacc_ptvarcone40.isAvailable(*muon_xAOD) ? 
    cacc_ptvarcone40(*muon_xAOD) * m_convertFromMeV : -1.0;

  // TTVA ptvarcone

    
  mu->ptvarcone30_TightTTVA_pt1000 = 
    cacc_ptvarcone30_TightTTVA_pt1000.isAvailable(*muon_xAOD) ?
    cacc_ptvarcone30_TightTTVA_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;
  mu->ptvarcone30_TightTTVA_pt500 =   
    cacc_ptvarcone30_TightTTVA_pt500.isAvailable(*muon_xAOD) ?    
    cacc_ptvarcone30_TightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  mu->ptvarcone20_TightTTVA_pt1000 = 
    cacc_ptvarcone20_TightTTVA_pt1000.isAvailable(*muon_xAOD) ?
    cacc_ptvarcone20_TightTTVA_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;
  mu->ptvarcone20_TightTTVA_pt500 =   
    cacc_ptvarcone20_TightTTVA_pt500.isAvailable(*muon_xAOD) ?    
    cacc_ptvarcone20_TightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  mu->ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500 =   
    cacc_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500.isAvailable(*muon_xAOD) ?    
    cacc_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  // TTVA ptcone
  mu->ptcone20_TightTTVALooseCone_pt500 = 
    cacc_ptcone20_TightTTVALooseCone_pt500.isAvailable(*muon_xAOD) ?
    cacc_ptcone20_TightTTVALooseCone_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  mu->ptcone20_TightTTVALooseCone_pt1000 = 
    cacc_ptcone20_TightTTVALooseCone_pt1000.isAvailable(*muon_xAOD) ?
    cacc_ptcone20_TightTTVALooseCone_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;

  mu->ptcone20_TightTTVA_pt500 = 
    cacc_ptcone20_TightTTVA_pt500.isAvailable(*muon_xAOD) ?
    cacc_ptcone20_TightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  mu->ptcone20_TightTTVA_pt1000 = 
    cacc_ptcone20_TightTTVA_pt1000.isAvailable(*muon_xAOD) ?
    cacc_ptcone20_TightTTVA_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;

  mu->ptcone20_Nonprompt_All_MaxWeightTTVA_pt500 =   
    cacc_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500.isAvailable(*muon_xAOD) ?    
    cacc_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;
  
  // PFlow isocone

  mu->neflowisol20 = 
    cacc_neflowisol20.isAvailable(*muon_xAOD) ?
    cacc_neflowisol20(*muon_xAOD) * m_convertFromMeV : -1.0;

  // Trigger matching result
  mu->triggerMap = trigMap;

  // Reconstruction scale factors
  mu->recoSF = recoSF;

  // Trigger efficiencies
  if(trigEff) mu->trigEff = *trigEff;

  // Precision MS hits
  mu->nPrecLayers     = cacc_nPrecisionLayers.isAvailable( *track ) ? cacc_nPrecisionLayers( *track ) : -1.0;
  mu->nPrecHoleLayers = cacc_nPrecisionHoleLayers.isAvailable( *track ) ? cacc_nPrecisionHoleLayers( *track ) : -1.0;
  mu->nGoodPrecLayers = cacc_nGoodPrecisionLayers.isAvailable( *muon_xAOD ) ? cacc_nGoodPrecisionLayers( *muon_xAOD ) : -1.0;

  // ID and ME track measurements
  mu->idPt         = cacc_InnerDetectorPt.isAvailable( *muon_xAOD ) ? cacc_InnerDetectorPt( *muon_xAOD ) * m_convertFromMeV : -1.0;
  mu->mePt         = cacc_MuonSpectrometerPt.isAvailable( *muon_xAOD ) ? cacc_MuonSpectrometerPt( *muon_xAOD ) * m_convertFromMeV  : -1.0;
  mu->idTheta      = idtrack ?  idtrack->theta() : -999.0;
  mu->meTheta      = metrack ?  metrack->theta() : -999.0;
  mu->idQoverPErr  = idtrack ? idtrack->definingParametersCovMatrix()(4,4) : -999.0;
  mu->meQoverPErr  = metrack ? metrack->definingParametersCovMatrix()(4,4) : -999.0;
  mu->cbQoverPErr  = track ? track->definingParametersCovMatrix()(4,4) : -999.0;

  if (idtrack) {
    idtrack->summaryValue(mu->nIBLHits, xAOD::numberOfInnermostPixelLayerHits);
    idtrack->summaryValue(mu->nPixHits, xAOD::numberOfPixelHits);
  } else {
    mu->nIBLHits = -1;
    mu->nPixHits = -1;
  }

  // Energy loss
  float ParamEnergyLoss(-9999.99), MeasEnergyLoss(-9999.99);

  muon_xAOD->parameter(ParamEnergyLoss,xAOD::Muon::ParamEnergyLoss);
  muon_xAOD->parameter(MeasEnergyLoss,xAOD::Muon::MeasEnergyLoss);

  mu->ParamEnergyLoss = ParamEnergyLoss * m_convertFromMeV;
  mu->MeasEnergyLoss  = MeasEnergyLoss * m_convertFromMeV;

  // Combined chi2/nDOF
  mu->matchQ = track ? (track->chiSquared() / track->numberDoF() ) : -999.0;

  // For truth info
  bool filledTruthTypeOrigin = false;

  // Save Truth TLV 
  const xAOD::TruthParticle* truthMuon = xAOD::TruthHelpers::getTruthParticle( *track );
  // the TruthLink between muon track and truth muon seems not to be present
  // in e.g. DAOD_PHYS, so try also with the xAOD::Muon
  if (!truthMuon){
    truthMuon = xAOD::TruthHelpers::getTruthParticle( *muon_xAOD );
    // verify validity of the truth particle's aux container (truthParticleLinks may be broken in derivation)
    if (truthMuon && !truthMuon->isAvailable<float>("e")) {
      truthMuon = 0;
    }
  }

  if( truthMuon ){
    mu->truthTLV.SetPtEtaPhiM( truthMuon->pt() * m_convertFromMeV,
                               truthMuon->eta(),
                               truthMuon->phi(),
                               Constants::MUON_MASS * m_convertFromMeV );

    mu->truthQ = truthMuon->charge();
    mu->barcode = truthMuon->barcode();

    if( truthMuon->isAvailable<unsigned int>("classifierParticleType") && truthMuon->isAvailable<unsigned int>("classifierParticleOrigin") ){
      filledTruthTypeOrigin = true;
      mu->type   = truthMuon->auxdata<unsigned int>("classifierParticleType");
      mu->origin = truthMuon->auxdata<unsigned int>("classifierParticleOrigin");
    }
  }

  // If there was no truth particle associated to the reco object OR if the
  // classifierParticleType was UnknownMuon, let's try checking the reco object's
  // truth information.
  if( !filledTruthTypeOrigin || mu->type == 5 ){
    if(track){
      mu->type   = xAOD::TruthHelpers::getParticleTruthType(*track);
      mu->origin = xAOD::TruthHelpers::getParticleTruthOrigin(*track);
    }
    else{
      mu->type   = -999;
      mu->origin = -999;
    }
  }

  unsigned int IFFtype(99);
  if(!m_isData)CHECK(m_baseObject->IFFClassifier->classify(*muon_xAOD,IFFtype));
  mu->iff_type = IFFtype;
  

  // PromptLeptonTagging variables
  mu->promptLepVeto_score =
    cacc_promptLepVeto.isAvailable( *muon_xAOD ) ? cacc_promptLepVeto( *muon_xAOD )  : 0;  
  mu->LowPtPLV_score =
    cacc_LowPtPLV.isAvailable( *muon_xAOD ) ? cacc_LowPtPLV( *muon_xAOD )  : 0;  

  // Detailed muon information
  fillDetailedMuonVariables(muon_xAOD,mu);

  // Save
  it->second->push_back(mu);

  Timer::Instance()->End( "MuonObject::fillMuonContainer" );

}
// ------------------------------------------------------------------------- //
void MuonObject::fillDetailedMuonVariables(xAOD::Muon* muon_xAOD,MuonVariable*& mu)
{

  // Don't fill unless user wants
  if( !m_writeDetailedSkim ) return;

  // Muon track variables
  const xAOD::TrackParticle* track   = muon_xAOD->primaryTrackParticle();
  if(track)
    mu->trackTLV.SetPtEtaPhiM( track->pt() * m_convertFromMeV,
			       track->eta(),
			       track->phi(),
			       Constants::MUON_MASS * m_convertFromMeV );
  
  // PromptLeptonTagger variables  
  mu->plt_input_TrackJetNTrack =
    cacc_promptLepInput_TrackJetNTrack.isAvailable( *muon_xAOD ) ?  cacc_promptLepInput_TrackJetNTrack( *muon_xAOD ) : 0;  
  mu->plt_input_DRlj =
    cacc_promptLepInput_DRlj.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_DRlj( *muon_xAOD ) : 0;
  mu->plt_input_rnnip =
    cacc_promptLepInput_rnnip.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_rnnip( *muon_xAOD ) : 0;
  mu->plt_input_DL1mu =
    cacc_promptLepInput_DL1mu.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_DL1mu( *muon_xAOD ) : 0;
  mu->plt_input_PtRel =
    cacc_promptLepInput_PtRel.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_PtRel( *muon_xAOD ) : 0;
  mu->plt_input_PtFrac =
    cacc_promptLepInput_PtFrac.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_PtFrac( *muon_xAOD ) : 0;
  mu->plt_input_Topoetcone30Rel =
    cacc_promptLepInput_Topoetcone30Rel.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_Topoetcone30Rel( *muon_xAOD ) : 0;
  mu->plt_input_Ptvarcone30Rel =
    cacc_promptLepInput_Ptvarcone30Rel.isAvailable( *muon_xAOD ) ? cacc_promptLepInput_Ptvarcone30Rel( *muon_xAOD ) : 0;
  
}
// --------------------------------------------------------------------------------------------------------------------------
void MuonObject::fillMuonContainerTruth(xAOD::TruthParticle* muon_xAOD, std::string sys_name /*=""*/)
{

  std::map<TString,MuonVector*>::iterator it = m_muonVectorMap.find(sys_name);

  if(it==m_muonVectorMap.end()){
    std::cout << "<MuonObject::fillMuonContainer> ERROR Request to get muon for unknown systematic: " << sys_name << std::endl;
    return;
  }

  MuonVariable* mu = new MuonVariable();

  // TLV
  mu->SetPtEtaPhiM( muon_xAOD->pt() * m_convertFromMeV,
		    muon_xAOD->eta(),
		    muon_xAOD->phi(),
		    muon_xAOD->m() * m_convertFromMeV );
  
  mu->q = muon_xAOD->charge();

  // TODO: Defaults for all variables, using truth information when required //

  // SUSYTools defs **TODO**
  mu->signal = true;
  mu->passOR = true;

  // Set isolation and impact parameter
  // to perfect values for truth muon
  mu->ptcone20 = 0.0;
  mu->ptcone30 = 0.0;
  mu->etcone30 = 0.0;
  mu->d0     = 0.0;
  mu->z0     = 0.0;
  mu->d0Err  = 0.0;
  mu->z0Err  = 0.0;

  // Should be muons
  mu->type      = 6; 
  mu->origin    = muon_xAOD->auxdata<int>("motherID"); 

  if( muon_xAOD->isAvailable<unsigned int>("classifierParticleType") && muon_xAOD->isAvailable<unsigned int>("classifierParticleOrigin") ){
      mu->type   = muon_xAOD->auxdata<unsigned int>("classifierParticleType");
      mu->origin = muon_xAOD->auxdata<unsigned int>("classifierParticleOrigin");
  }

  // TODO: does this need to be stored?
  //mu->author   = muon_xAOD->author();

  // TODO: This should come from the tool
  //mu->quality  = muon_xAOD->quality(); // this will probabaly not exist in DxAODs

  // set scale factor to 1.0
  std::map<TString, float> recoSF;
  recoSF[""] = 1.0;
  mu->recoSF = recoSF;

  it->second->push_back(mu);
  
}
// ------------------------------------------------------------------------- //
void MuonObject::fillMuonContainer_NearbyLepIsoCorr(xAOD::Muon* muon_xAOD, unsigned int index_baseline_mu, std::string sys_name)
{

  Timer::Instance()->Start( "MuonObject::fillMuonContainer_NearbyLepIsoCorr" );
  
  std::map<TString,MuonVector*>::iterator it = m_muonVectorMap.find(sys_name);
  
  if( it==m_muonVectorMap.end() ){
    std::cout << "<MuonObject::fillMuonContainer_NearbyLepIsoCorr> ERROR Request to get muon for unknown systematic: " << sys_name << std::endl;
    return;
  }
  
  // Get a reference to the MuonVariable
  MuonVariable* muon = it->second->at(index_baseline_mu);
  
  // Redefine signal criteria to be pre-isolation signal && corrected isolation
  muon->signal  = cacc_signal( *muon_xAOD ) && muon_xAOD->auxdata<char>("isol_corr");
  
  // Isolation from selector tools
  const asg::AcceptData isIsoSel          = m_baseObject->isoTool->accept(*muon_xAOD);

  //Old WPs
  muon->corr_IsoFCTightTrackOnly        = isIsoSel.getCutResult("FCTightTrackOnly");
  muon->corr_IsoFCLoose                 = isIsoSel.getCutResult("FCLoose");
  muon->corr_IsoFCTight                 = isIsoSel.getCutResult("FCTight");
  muon->corr_IsoFCLoose_FixedRad        = isIsoSel.getCutResult("FCLoose_FixedRad");
  muon->corr_IsoFCTight_FixedRad        = isIsoSel.getCutResult("FCTight_FixedRad");

  muon->corr_IsoHighPtTrackOnly         = isIsoSel.getCutResult("HighPtTrackOnly"); 
  muon->corr_IsoTightTrackOnly_VarRad   = isIsoSel.getCutResult("TightTrackOnly_VarRad");
  muon->corr_IsoTightTrackOnly_FixedRad = isIsoSel.getCutResult("TightTrackOnly_FixedRad");
  muon->corr_IsoLoose_VarRad            = isIsoSel.getCutResult("Loose_VarRad");
  muon->corr_IsoLoose_FixedRad          = isIsoSel.getCutResult("Loose_FixedRad");
  muon->corr_IsoTight_VarRad            = isIsoSel.getCutResult("Tight_VarRad");
  muon->corr_IsoTight_FixedRad          = isIsoSel.getCutResult("Tight_FixedRad");
  //muon->corr_IsoPflowLoose_VarRad       = isIsoSel.getCutResult("PflowLoose_VarRad");  
  //muon->corr_IsoPflowLoose_FixedRad     = isIsoSel.getCutResult("PflowLoose_FixedRad");
  //muon->corr_IsoPflowTight_VarRad       = isIsoSel.getCutResult("PflowTight_VarRad");  
  //muon->corr_IsoPflowTight_FixedRad     = isIsoSel.getCutResult("PflowTight_FixedRad");
 
  // Retrieve & set isolation from xAODs

  // ptcone
  float ptcone20 = 0.0;
  float ptcone30 = 0.0;
  float ptcone40 = 0.0;  
  muon_xAOD->isolation(ptcone20,xAOD::Iso::ptcone20);
  muon_xAOD->isolation(ptcone30,xAOD::Iso::ptcone30);
  muon_xAOD->isolation(ptcone40,xAOD::Iso::ptcone40);
  muon->corr_ptcone20 = ptcone20 * m_convertFromMeV;
  muon->corr_ptcone30 = ptcone30 * m_convertFromMeV;
  muon->corr_ptcone40 = ptcone40 * m_convertFromMeV;

  //  topoetcone
  float topoetcone20 = 0.0;
  float topoetcone30 = 0.0;
  float topoetcone40 = 0.0;
  muon_xAOD->isolation(topoetcone20,xAOD::Iso::topoetcone20);
  muon_xAOD->isolation(topoetcone30,xAOD::Iso::topoetcone30);
  muon_xAOD->isolation(topoetcone40,xAOD::Iso::topoetcone40);
  muon->corr_topoetcone20 = topoetcone20 * m_convertFromMeV;
  muon->corr_topoetcone30 = topoetcone30 * m_convertFromMeV;
  muon->corr_topoetcone40 = topoetcone40 * m_convertFromMeV;

  //  ptvarcone
  muon->corr_ptvarcone20 = cacc_corr_ptvarcone20.isAvailable( *muon_xAOD ) ? 
    cacc_corr_ptvarcone20(*muon_xAOD) * m_convertFromMeV : -1.0;
  muon->corr_ptvarcone30 = cacc_corr_ptvarcone30.isAvailable( *muon_xAOD ) ? 
    cacc_corr_ptvarcone30(*muon_xAOD) * m_convertFromMeV : -1.0;
  muon->corr_ptvarcone40 = cacc_corr_ptvarcone40.isAvailable( *muon_xAOD ) ? 
    cacc_corr_ptvarcone40(*muon_xAOD) * m_convertFromMeV : -1.0;

  // TTVA ptvarcone
  
  muon->corr_ptvarcone30_TightTTVA_pt1000 = 
    cacc_corr_ptvarcone30_TightTTVA_pt1000.isAvailable(*muon_xAOD) ?
    cacc_corr_ptvarcone30_TightTTVA_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;
  muon->corr_ptvarcone30_TightTTVA_pt500 =   
    cacc_corr_ptvarcone30_TightTTVA_pt500.isAvailable(*muon_xAOD) ?    
    cacc_corr_ptvarcone30_TightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  muon->corr_ptvarcone20_TightTTVA_pt1000 = 
    cacc_corr_ptvarcone20_TightTTVA_pt1000.isAvailable(*muon_xAOD) ?
    cacc_corr_ptvarcone20_TightTTVA_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;
  muon->corr_ptvarcone20_TightTTVA_pt500 =   
    cacc_corr_ptvarcone20_TightTTVA_pt500.isAvailable(*muon_xAOD) ?    
    cacc_corr_ptvarcone20_TightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  // muon->corr_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500 =   
  //   cacc_corr_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500.isAvailable(*muon_xAOD) ?    
  //   cacc_corr_ptvarcone30_Nonprompt_All_MaxWeightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  muon->corr_ptcone20_TightTTVALooseCone_pt500 = 
    cacc_corr_ptcone20_TightTTVALooseCone_pt500.isAvailable(*muon_xAOD) ?
    cacc_corr_ptcone20_TightTTVALooseCone_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;

  muon->corr_ptcone20_TightTTVALooseCone_pt1000 = 
    cacc_corr_ptcone20_TightTTVALooseCone_pt1000.isAvailable(*muon_xAOD) ?
    cacc_corr_ptcone20_TightTTVALooseCone_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;
  
  muon->corr_ptcone20_TightTTVA_pt500 = 
    cacc_corr_ptcone20_TightTTVA_pt500.isAvailable(*muon_xAOD) ?
    cacc_corr_ptcone20_TightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;
  
  muon->corr_ptcone20_TightTTVA_pt1000 = 
    cacc_corr_ptcone20_TightTTVA_pt1000.isAvailable(*muon_xAOD) ?
    cacc_corr_ptcone20_TightTTVA_pt1000(*muon_xAOD) * m_convertFromMeV : -1.0;

  // muon->corr_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500 =   
  //   cacc_corr_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500.isAvailable(*muon_xAOD) ?    
  //   cacc_corr_ptcone20_Nonprompt_All_MaxWeightTTVA_pt500(*muon_xAOD) * m_convertFromMeV : -1.0;
 
  // pflow isolation
  // muon->corr_neflowisol20 =   
  //   cacc_corr_neflowisol20.isAvailable(*muon_xAOD) ?    
  //   cacc_corr_neflowisol20(*muon_xAOD) * m_convertFromMeV : -1.0;

  // Save muon
  it->second->at(index_baseline_mu) = muon;

  Timer::Instance()->End( "MuonObject::fillMuonContainer_NearbyLepIsoCorr" );

}
// ------------------------------------------------------------------------- //
int MuonObject::getMuonQuality(xAOD::Muon* muon_xAOD)
{

  int quality = -1;

  // Quality of the muon, coming from MST
  // Naming scheme is the following:
  //   -> [0,4]   quality, when high pT WP isn't passed
  //   -> [10,14] quality + 10, when high pT WP *IS* passed
  xAOD::Muon::Quality muonQuality = m_muonSelectionTool->getQuality( *muon_xAOD );
  if( muonQuality <= xAOD::Muon::Tight          ) quality = 0;
  else if( muonQuality <= xAOD::Muon::Medium    ) quality = 1;
  else if( muonQuality <= xAOD::Muon::Loose     ) quality = 2;
  else if( muonQuality <= xAOD::Muon::VeryLoose ) quality = 3;
  else{
    std::cout << "<MuonObject::getMuonQuality> WARNING Unknown muon quality from the MuonSelectorTool. Defaulting to -1" << std::endl;
    quality = -1;
  }

  // High pT 
  if( m_muonSelectionTool->passedHighPtCuts( *muon_xAOD ) ){
    quality += 10;
  }

  return quality;

}
// ------------------------------------------------------------------------- //
const MuonVector* MuonObject::getObj(TString sysName) 
{

  std::map<TString,MuonVector*>::iterator it = m_muonVectorMap.find(sysName);

  if(it==m_muonVectorMap.end()){
    std::cout << "<MuonObject::getObj> WARNING Cannot get muon vector for systematic: " << sysName << std::endl;
    return NULL;
  }

  return it->second;
  
}
// ------------------------------------------------------------------------- //
void MuonObject::Reset()
{

  std::map<TString,MuonVector*>::iterator it;
  for(it = m_muonVectorMap.begin(); it != m_muonVectorMap.end(); it++){

    // Free up memory
    for (MuonVector::iterator muItr = it->second->begin(); muItr != it->second->end(); muItr++) {
      if( (*muItr)->trkLink ) delete (*muItr)->trkLink; 
      delete *muItr;
    }

    it->second->clear();
  }

}
// ------------------------------------------------------------------------- //
MuonObject::~MuonObject()
{

  if( m_muonSelectionTool ) delete m_muonSelectionTool; 
  if( m_baseObject        ) delete m_baseObject;


}
