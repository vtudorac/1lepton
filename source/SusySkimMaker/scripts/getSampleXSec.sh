#!/bin/bash

#
# CHECK THAT THE USER PASSED A FILE TO THIS SCRIPT
#
if [[ $# > 0 ]]; then
        input=$1
else
        echo "Usage ./getSampleStatus.sh <file>" 
        exit
fi

#
# MAKE SURE AMI IS SETUP, 
#
if ! type "ami" > /dev/null; then
  echo "Please setup AMI (localSetupPyAMI) and be sure that you have a valid grid proxy (voms-proxy-init -voms atlas)"
  exit
fi

ALL_SAMPLES_OK=1

lsetup  "asetup AthAnalysisBase,2.4.35" pyAMI

rm -rf "tmp.txt"

for ds in `cat $input | awk '{print $1}'`; do

        if [[ $ds == *#* ]]; then
          continue
        fi

        # AMI doesn't like the scope that ruico and DQ2 have upgraded to
        if [[ $ds == *:* ]]; then
          ds=`echo $ds | awk -F ":" '{print $2}' `
        fi

        echo $ds >> "tmp.txt"

done

#
getMetadata.py --fields="dataset_number,physics_short,crossSection_pb,kFactor,genFiltEff,crossSectionTotalRelUncertUP" --inDsTxt="tmp.txt"
rm -rf "tmp.txt"

