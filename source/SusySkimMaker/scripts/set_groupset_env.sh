#!/bin/bash

#
# Written by Matthew Gignac (UBC, 2016)
#

# Modify this script as needed
# to contain the information below
# These environment variables will be 
# found and used instead of specifying 
# them each time you want to 
# interact with the group-set

export GROUP_SET_NAME=""
export GROUP_SET_DIR=""
export GROUP_SET_TAG=""
export GROUP_SET_DEEPCONFIG=""

