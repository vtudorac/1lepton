#!/usr/bin/env python

#
# Script to check download logs created from run_download
#
# Written by: Matthew Gignac (2016, UBC)
#

import ROOT
import os,sys,argparse

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-d', dest='tree_path',help='Path to the tree directory of your download')
args = parser.parse_args()
tree_path=args.tree_path

# Check for a valid tree path
if not os.path.isdir(tree_path) :
  sys.exit( "ERROR: Invalid path %s!!!" % tree_path )
 
#
print 'Checking for incomplete downloads...'

#
for root, dirs, filenames in os.walk(tree_path):
    for f in filenames:
        # Consider only download_log files
        if 'download_log' not in f :
            continue

        # Open file and check for incomplete downloads
        log = open(root+f,'r')
        
        #print 'Reading log %s'%f
        for line in log:
            if 'Files that cannot be downloaded' in line :

                s,n=line.split(":")
                n=int(n.replace(" ",""))

                if n>0:
                    print 'Sample %s has %i file which could not be downloaded!' %(f,n)
