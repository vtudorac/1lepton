#ifndef SusySkimMaker_MuonObject_h
#define SusySkimMaker_MuonObject_h

// Rootcore
#include "SusySkimMaker/BaseHeader.h"
#include "SusySkimMaker/BaseObject.h"
#include "SusySkimMaker/MuonVariable.h"
#include "SusySkimMaker/TrackObject.h"

// xAOD
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"

// xAOD Tools
#include "MuonSelectorTools/MuonSelectionTool.h"

// Forward declarations
class TreeMaker;


typedef std::vector<MuonVariable*> MuonVector;

class MuonObject 
{

/*
 >>>>>>>>>>>>>>>
  Work in progress. Changing organization of systematics, such that tools 
  created in this class (outside SUSYTools) can be configured
*/

 private:
  // Under testing
   CP::SystematicSet m_sysSet;

 public:
   void setSystematicSet(const CP::SystematicSet sysSet){ m_sysSet=sysSet; }; 

/*
 <<<<<<<<<<<<<<<<<
*/

 private:

  ///
  /// Initialize all tools needed for this class
  ///
  StatusCode init_tools();

 public:
 
  ///
  /// Default constructor
  ///
  MuonObject();

  ///
  /// Destructor
  ///
  virtual ~MuonObject();

  ///
  /// Initialization method for this class. Connects each MuonVariable in global m_muonVectorMap map
  /// to output trees found in TreeMaker class
  ///
  StatusCode init(TreeMaker*& treeMaker, bool isData);

  ///
  /// Fill MuonVariable member variables for a systematic sys_name
  ///
  void fillMuonContainer(xAOD::Muon* mu_xAOD,
                        const xAOD::VertexContainer* primVertex,
                        const xAOD::EventInfo* eventInfo,
                        const std::map<TString,bool> trigMap, 
                        const std::map<TString,float> recoSF,
                        MCCorrContainer* trigEff,
                        std::string sys_name);


  ///
  /// Fill into MuonVariable more maniac ones: i.e. input variables to PromptLeptonTagger etc
  ///
  void fillDetailedMuonVariables(xAOD::Muon* muon_xAOD, MuonVariable*& mu);


  ///
  /// Similiar method to create muon object from truth information
  ///
  void fillMuonContainerTruth(xAOD::TruthParticle* muon_xAOD, std::string sys_name="");

  ///
  /// Similiar method to modify MuonVariable with NearbyLepIsoCorr values
  ///
  void fillMuonContainer_NearbyLepIsoCorr(xAOD::Muon* mu_xAOD,
                                          unsigned int index_baseline_mu,
                                          std::string sys_name);
  
  // Track object tol
  void setTrackObject(TrackObject* trackObject){ m_trackObject=trackObject; }

  ///
  /// Return a std::vector< MuonVariable* > (alias MuonVector) for a systematic sysName. 
  /// If no MuonVector is found, return a null MuonVector
  ///
  const MuonVector* getObj(TString sysName); 

  ///
  /// Clear internal MuonVectors inside m_muonVectorMap 
  /// 
  void Reset();
 
 protected:

  ///
  /// Internal naming scheme for muon quality
  ///
  int getMuonQuality(xAOD::Muon* muon_xAOD);

  /// 
  /// Global std::map to store MuonVectors for all systematic uncertainties
  ///
  std::map<TString,MuonVector*>   m_muonVectorMap;

  ///
  /// Muon Selector tool, only used for raw objects right now
  ///
  CP::MuonSelectionTool*           m_muonSelectionTool;

  /// 
  /// Convert units from MeV
  ///
  float                           m_convertFromMeV;  

  ///
  /// Is data?
  ///
  bool m_isData;
  
  ///
  /// Is Run3?
  ///
  bool m_isRun3;

  ///
  /// Interface to isolation tools, and in principle can be expanded
  ///
  BaseObject* m_baseObject;

  ///
  /// Trackobj tool
  TrackObject* m_trackObject;

  ///
  /// Write out additional variables, typically only for specific studies
  /// so turned off by default. Can be steering by deep.conf
  ///
  bool m_writeDetailedSkim;

};

#endif
