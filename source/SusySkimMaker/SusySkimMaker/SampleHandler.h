#ifndef SusySkimMaker_SampleHandler_h
#define SusySkimMaker_SampleHandler_h

#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <map>

#include "TString.h"
#include "TSystem.h"
#include "TChain.h"
#include "TFile.h"

#include "SusySkimMaker/CheckGridSubmission.h"
#include "SusySkimMaker/DuplicateEventChecker.h"

// Possibly replace with Observables, when intergrating tt-reweighting as a merge option
#include "SusySkimMaker/EventObject.h"

// Forward declarations
class ObjectTools;
class SampleSet;

//
class SampleHandler{

 public:

  ///
  /// Default constructor
  ///
  SampleHandler();

  ///
  /// Default destructor
  ///
  ~SampleHandler();

  // Configure this class, needed for merging, downloading, etc
  bool initialize(TString user_path,TString tag,TString groupSet, TString prefix);

  // Configure only for using the sample this class collects
  bool initialize(TString groupSet, TString tag, TString prefix = "user.*");

  // Called from the initialize methods to set the required variables
  //  - Field has a non-null value, nothing is done
  //  - Field is whitespace, try to extract its value from an environment variable called env
  //  - If no env variable is found, print out errorMsg and abort.
  static void setVariable(TString& field, TString env, TString errorMsg);

  //
  // Enum to specify which batch system to submit too. This can be expanded as
  // more batch systems are included.
  ///
  enum BATCH_TYPE{
    NONE=0,
    LXPLUS=1,
    TORQUE=2,
    LRZ=3,
    CONDOR=4,
    ETP=5,
    LSF=6
  };

  ///
  /// Properties for different systematic configurations
  /// One job with all samples found in SampleConfig is submmitted
  ///
  struct SysConfig{
    TString systematic;
    int writeTrees;
    int writeSkims;
    int JESNPSet;
  };

  ///
  /// Any properties of our samples
  /// TODO: SampleSet will replace this class
  ///
  struct SampleConfig{
    std::vector<TString> samples;
    std::vector<TString> mergeInputs;

    //
    TString file;     //single file/url to run on
    TString fileList; //list of files/urls to run on
    TString process;  // ultimately will become the treename

    //
    bool isData;
    bool isAF2;
    bool isTruthOnly;

    //
    int priority;

    // PRW on-the-fly information
    TString prwNom_rootFilePath;
    TString prwDown_rootFilePath;
    TString prwUp_rootFilePath;

    // Number of duplicate events, by DSID
    // Fill when calling countDuplicateEvents
    // Key will correspond to tree name
    // Value is a vector of pairs, key==DSID and value==number of duplicates
    std::map<TString, std::vector< std::pair<unsigned int,double>> > duplicateEvtSF;

    void addDuplicateEvtSF(TString treeName, std::vector< std::pair<unsigned int,double>> SFs){
      duplicateEvtSF.insert( std::pair<TString, std::vector< std::pair<unsigned int,double>> >(treeName,SFs) );
    }


    void print() const{
      std::cout << "Process name  : " << process << std::endl;
      std::cout << "Is data?      : " << isData << std::endl;
      std::cout << "Is Fast-sim?  : " << isAF2 << std::endl;
      std::cout << "Listing samples..." << std::endl;
      for(auto& sample : samples ){
        std::cout << "  >>> " << sample << std::endl;
      }
    }

  };

  ///
  /// Overall sample configuration
  ///
  struct SampleInfo{
    TString groupName;
    TString SUSYToolsConfig;
    TString analysisPackage;
    TString deepConfigFile;
    TString dirName;
    std::vector<SampleConfig*> sampleConfig;
    std::vector<SysConfig*> sysConfig;

    void addSysSet(TString systematic, int writeSkims, int writeTrees){
      SysConfig* sysSet = new SysConfig();
      sysSet->systematic  = systematic;
      sysSet->writeTrees  = writeTrees;
      sysSet->writeSkims  = writeSkims;
      sysConfig.push_back(sysSet);
    }

  };

  /// This method will define sets of mc or data that the user can select
  ///
  void defineSets(TString groupSet);
  SampleHandler::SampleInfo* readGroupSetConfig(TString fileName);
  static std::vector<TString> getGroupSetList();

  ///
  /// Collect all samples and call below functions
  ///
  void addSamples(SampleInfo*& sampleInfo,TString groupSet,TString path="");
  void addSampleConfig(SampleInfo*& sampleInfo,TString textFileName, TString path="");
  void addSampleConfig(SampleInfo*& sampleInfo,SampleConfig*& master_config,TString textFileName, TString path="");

  ///
  /// TODO: Move to CheckGridSubmission
  ///
  StatusCode checkSystematicSet(TString sysSet);

  ///
  /// Set a global group name, so any samples you add via addSamples(...) will be associated
  /// to this group name, and stored into the m_samples map with key==groupName
  /// Likewise, any systematic sets you add via addSystematicSet(...) will be associated with this
  /// global group name.
  ///
  void addGroup(SampleInfo*& sampleInfo);

  ///
  /// Look for a SampleInfo struct with key=groupName. In the event its not found, abort.
  ///
  SampleInfo* getSampleInfo(TString groupName);
  SampleConfig* readHeader(TString textFileName, TString path);

  ///
  /// Batch system methods. Can be expanded easily. Add a new function similar to submitToLxPlus
  /// and call this function from submitByGroupSet adding a new BATCH_TYPE above.
  ///
  void submitByGroupSet(TString selector, BATCH_TYPE batch_type);
  void submitToBatch(std::vector<TString> samples, TString output, TString selector, BATCH_TYPE batch_type, int nSkip=0, int nEvt=-1);
  void submitToLxPlus(TString input,TString output,TString selector,int nSkip=0, int nEvt=-1);
  void submitToTorque(TString input,TString output,TString selector,int totalNumberOfJobs,int nSkip=0, int nEvt=-1);
  void submitToLRZ(TString input,TString output,TString selector,int totalNumberOfJobs,int nSkip=0, int nEvt=-1);
  void submitToCondor(TString input,TString output,TString selector,int nSkip=0, int nEvt=-1);
  void submitToETP(TString input,TString output,TString selector,int nSkip=0, int nEvt=-1);
  void setMaxQueuedJobs(int maxQueuedJobs){ m_maxQueuedJobs=maxQueuedJobs; }
  void setMaxEventsPerJob(int maxEventsPerJob){ m_maxEventsPerJob=maxEventsPerJob; }

  ///
  /// Merge datasets from a particular group set.
  ///
  void prepareMergeGroupSet();

  ///
  /// Dump the cross section (xsec,k-factor, generator efficiency, etc) and number of processed events
  ///
  void dumpSampleInfo(TString tag);

  /// Call the two functions below
  void countDuplicateAndMissingEvents(const SampleSet* sampleSet, TTree*& tree, TChain* cbkChain);

  /// Duplicate event checkings and functionality
  static void countDuplicateEvents(DuplicateEventChecker*& dec,const SampleSet* sampleSet, TTree*& tree);

  /// To check for missing events
  static void checkMissingEvents(DuplicateEventChecker*& dec,const SampleSet* sampleSet, TChain* _cbkChain);

  ///
  /// Download functions
  ///
  void download();
  void downloadPRW();
  TString getDSName(TString fullName,TString tag,TString sysSet="",bool isData=false,int writeSkims=-1, int writeTrees=-1);
  TString getDSNameSuffix(TString tag,TString sysSet, int writeSkims, int writeTrees);
  TString getDSIDFromSampleName(TString fullSample);

  // Methods for cross checks
  void checkMetaData(TString grid_sample,TString local_sample,TString histName,TString binName);
  StatusCode check(bool submitToGrid,TString groupSet,TString selector,bool checkAllSamples);
  int getNumEventsAMI(TString sample);
  float getNumEventsCounterHist(TString rootFileName,TString histName,TString binName);

  // Set flags and helper functions.
  void disableSkims(bool disableSkims){m_disableSkims=disableSkims;}
  void disableTrees(bool disableTrees){m_disableTrees=disableTrees;}
  void disableMetaDataCheck(bool disableMetaDataCheck){m_disableMetaDataCheck=disableMetaDataCheck;}
  void disablePRWCheck(bool disablePRWCheck){m_disablePRWCheck=disablePRWCheck;}
  void includeFiles(TString includeFiles){m_includeFiles=includeFiles;}
  void excludeFiles(TString excludeFiles){m_excludeFiles=excludeFiles;}
  void noSubmit(bool noSubmit){m_noSubmit=noSubmit;}
  void setLumi(float lumi){m_lumi=lumi;}
  void setSumOfWeightsPath(const char* sumOfWeightsPath){m_sumOfWeightsPath=sumOfWeightsPath;}
  void setXsecPath(const char* xsec){m_xsecPath=xsec;}
  TH1* getHist(TString rootFileName,TString histName);
  void setBatchStringfilter(TString batchStringfilter){m_batchStringfilter=batchStringfilter;}
  void setSplitLargeFiles(bool splitLargeFiles){m_splitLargeFiles=splitLargeFiles;}
  void isBulkSubmission( bool bulkSubmission ) {m_bulkSubmission=bulkSubmission; }
  void skipSampleInfoDump( bool skipSampleInfoDump ){ m_skipSampleInfoDump=skipSampleInfoDump; }
  void mergeByProcessName(bool mergeByProcessName){m_mergeByProcessName=mergeByProcessName;}
  void resubmitMergeJobs(bool resubmitMergeJobs){m_resubmitMergeJobs=resubmitMergeJobs;}
  void resubmitBatchJobs(bool resubmitBatchJobs){m_resubmitBatchJobs=resubmitBatchJobs;}
  void setSamplePriority(int priority){m_priority=priority;}
  void mergeOutputs(bool mergeOutputs){m_mergeOutputs=mergeOutputs;}
  void setPTag(TString ptag){m_pTag=ptag;}

  /// Comma separated list of tags
  void setPriorityTags(TString tags);

  ///
  /// Return the final state code for a given dataset number (needed for signal cross sections)
  ///
  int getFinalState(int DatasetNumber);

  static TString formatMergeOutputFileName(TString name,TString sys,bool isTruthOnly);
  static TString formatOutputTTreeName(TString inputTree,TString process, bool isData);


 private:

  void extractMetaData(MetaData*& metaData,TString sample);
  int getYear(int runNumber) {return runNumber <= 284484 ? 2015 : 2016;};

  ////////////////////

  //
  // Internal function called by public download
  //
  void download(const SampleInfo* set, TString tag, TString dir, TString stream);

  //
  void makeDirectoryStructure(TString user_path,TString tag,TString groupName);
  bool makeDirectory(TString path);

  ///
  /// This function should find the correct base directory for this class,
  /// For example, if you pass "somePath/groupSet/tag/skims/", this will
  /// remove groupSet/tag/skims/, as the base directory should be somePath
  ///
  void getBaseDirectory(TString& user_path,TString tag,TString groupName);

  ///
  ///
  ///
  bool getInputFiles(TString directory,TString tag,TString sample,SampleConfig*& sampleConfig,SampleSet*& sampleSet, bool doWarning=true);

  ///
  TString findTagForSample(TString sample,SampleConfig*& sampleConfig);


  // Search for a file
  // first look for it in the RootCorePath,
  // then try using PathResolver
  static TString getPath(TString fileName,TString RootCorePath, TString PathResolverPath);


  ///
  /// Work in progress, will be enabled with simple flag and used during the merge(..) stage
  /// This will allow to recalculate the pilup weights on the fly
  ///
  /// Current implementation
  ///   - Returns a string, which is the name of the output file
  ///     which one should use to link the tree,
  ///   - This function doesn't take care of the linking, because this function only needs to be
  ///     called once for systematics
 public:

  TString doPileupOnTheFly(TString outputDir, TString listOfDSID, bool isAF2, int var=0);
  void linkPRWFriendTree(TString filePath,TString process,TFile*& master_file,TTree*& master_tree);


  TString getMergeDir(){ return m_mergeDir; }
  TString groupName(){ return m_groupName; }
  std::vector<SampleSet*> getMergeSampleSets(){ return m_mergeSampleSets; }

 protected:

  ///
  /// Store every configuration of SampleInfo. Key is groupName
  ///
  std::map<TString,SampleInfo*> m_sampleInfo;

  ///
  /// Base directory where this class will store trees, skims and merged output
  ///
  TString m_baseDir;
  TString m_treeDir;
  TString m_localTreeDir;
  TString m_skimDir;
  TString m_mergeDir;
  TString m_sampleInfoDir;
  TString m_prwDir;

  ///
  /// pTag for a given derivation
  ///
  TString m_pTag;

  ///
  /// Name of your sample set
  ///
  TString m_groupName;

  ///
  /// Global tag identifier
  ///
  TString m_tag;

  ///
  /// Global prefix for the output container name. Usually something like user.<username> , defaults to user.*
  ///
  TString m_prefix;

  ///
  /// Indicates where the samples are placed. Directory read in my CentralDB class
  ///
  TString m_groupSetDir;

  /// Include or exclude file
  TString m_includeFiles;
  TString m_excludeFiles;
  TString m_batchStringfilter;

  ///
  /// Don't check metaData when merging
  ///
  bool m_disableMetaDataCheck;

  ///
  /// Don't check for valid PRW profiles when submitting to the grid
  ///
  bool m_disablePRWCheck;

  ///
  /// Disable downloading of skims
  ///
  bool m_disableSkims;

  ///
  /// Disable downloading of trees
  ///
  bool m_disableTrees;

  ///
  /// Turn off batch submission, dry run
  ///
  bool m_noSubmit;

  ///
  /// Luminosity for weight recalculation
  ///
  float m_lumi;

  ///
  /// Max jobs which are put into queue at one time
  ///
  int m_maxQueuedJobs;

  ///
  /// Maximum number of events per job before a new job is created
  ///
  int m_maxEventsPerJob;

  ///
  /// Path to sumOfWeightsHistogram for weight recalculation
  ///
  const char* m_sumOfWeightsPath;

  ///
  /// Path to SUSY xsec files for reweighting trees with new xsecs
  ///
  const char* m_xsecPath;

  ///
  /// Event Object - used as an interface to the SUSY xsec db
  ///
  EventObject* m_eventObject;
  ObjectTools* m_objectTools;

  ///
  /// DuplicateEventChecker
  ///
  DuplicateEventChecker* m_DEC;

 private:

  ///
  /// Internal flag, to ensure this class was configured
  ///
  int m_configured;

  ///
  /// When true, don't seperate process names containing a '%' into different sample configurations
  /// Default is false
  ///
  bool m_bulkSubmission;

  ///
  /// When true, don't dump the sample info. Default is false and will be done during the merge step
  ///
  bool m_skipSampleInfoDump;

  ///
  /// When submitting by groupSets locally, this flag is used to point the merge to the correct directory
  ///
  bool m_mergeByProcessName;

  ///
  /// Resubmit failed merge jobs to the batch system. Only implemented for certain batch systems.
  ///
  bool m_resubmitMergeJobs;
  bool m_resubmitBatchJobs;
  ///
  /// Set the priority of the sample. When set, only sampleConfigs with this value are collected
  ///
  int m_priority;

  ///
  /// Split large files/chains into separate jobs
  ///
  bool m_splitLargeFiles;

  ///
  /// Merge outputs into a single file when true, or split exactly how they arrived from the download
  /// Note that this can only be used when running locally (not supported for batch merges)
  ///
  bool m_mergeOutputs;

  ///
  /// Used internally to count the number of jobs
  ///
  int m_jobCounter;

  ///
  /// File to write out the submission commands (used for ETP where batch submission is not working out of SLC environment)
  ///
  std::ofstream m_jobcommands;


  ///
  /// When set, samples with this tag, and the same groupset,
  /// are used instead of the primary m_tag;
  std::vector<TString> m_priorityTags;


  std::map<int,int> m_duplicateEvents;
  std::vector<TString> triggerChains;

  //
  std::vector<SampleSet*> m_mergeSampleSets;

};


#endif
