#ifndef SusySkimMaker_MetaData_h
#define SusySkimMaker_MetaData_h

// ROOT
#include "TObject.h"
#include "TString.h"
#include "TH1F.h"

//C++
#include <map>
#include <utility>
#include <iostream>
#include <iomanip>

class MetaData : public TObject
{

 public:
  MetaData();
  virtual ~MetaData() {};

  MetaData(const MetaData&);
  MetaData& operator=(const MetaData&);

  /// 
  /// Key == className that uses this data
  /// value: key==description 
  std::map<TString,std::vector<std::pair<TString,TString>> > stringMetaData;
  
  ///
  /// Performance logs, from Timer class
  ///
  std::map<TString,TH1F*> timeLog;

  ///
  /// Keep track of any events we filter from passSherpaPtSliceOR method.
  /// Key is DSID number, and value is a pair with key event number and value the truth level vector boson pT
  ///
  std::map<unsigned int, std::vector< std::pair<unsigned int,float> >> sherpaOverlapEvents;
 

  ///
  /// Log this property. String in the main function, all other log methods
  /// call this after converting to a string
  ///
  void log(TString className,TString description,TString value); //!
  void log(TString className,TString description,float value); //!
  void log(TString className,TString description,int value); //!
  void log(TString className,TString description,bool value); //!

  ///
  /// Compare a MetaData object with this 
  ///
  bool compare(const MetaData* metaData);

  ///
  /// Print out everything stored in this class.
  ///
  void print() const;

  ClassDef(MetaData, 1);

};


#endif
