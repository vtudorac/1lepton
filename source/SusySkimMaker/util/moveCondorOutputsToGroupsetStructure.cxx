#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/ArgParser.h"

#include "TSystem.h"
#include "TString.h"
#include "TObjString.h"
#include <sys/stat.h>

using namespace std;

bool check(const char* inputFlag, const char* compar, const char* log)
{

  if (strcmp(inputFlag, compar) == 0   ) return true;
  if ((strcmp(inputFlag, "-help") ==0 || strcmp(inputFlag, "-h") ==0 )){
    printf("  => %-30s : %-50s \n",compar,log);
    return false;
  }
  return false;
}

int main(int argc, char** argv)
{

  const char* APP_NAME = "moveCondorOutputsToGroupsetStructure";

  TString baseDir      = "";
  TString groupSet     = "";
  TString deepConfig   = "";
  TString tag          = "";
  TString subdirPrefix = "";
  bool noSubmit        = false;

  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if (check(argv[i],"-d","Base directory for the outputs"))
      baseDir = argv[++i];
    else if (arg.parse(argv[i],"-groupSet","Group set name" ) )
      groupSet = argv[++i];
    else if (arg.parse(argv[i],"-deepConfig","Deep configuration file" ))
      deepConfig = argv[++i];
    else if (check(argv[i], "-tag","Specific a tag to be added to the data set name for grid jobs"))
      tag = argv[++i];
    else if (check(argv[i], "-subdirPrefix","Prefix for the subdirectories"))
      subdirPrefix = argv[++i];
    else
    {
      std::cout << "Unknown argument" << argv[i] << std::endl;
      return 0;
    }
  }

  // Absoutely required for all executables
  CHECK( PkgDiscovery::Init( deepConfig ) );

  SampleHandler* sh = new SampleHandler();
  sh->initialize(groupSet,"NONE");
  const SampleHandler::SampleInfo* sampleInfo = sh->getSampleInfo(groupSet);

  for( auto& sampleConfig : sampleInfo->sampleConfig ) {

    TString process = sampleConfig->process;

    for(auto& name : sampleConfig->samples){

      auto* nameTokens = name.Tokenize(".");

      TString dataOrMC = ((TObjString*)nameTokens->At(0))->String();
      bool isData = dataOrMC.Contains("data");

      TString dsid = ((TObjString*)nameTokens->At(1))->String();
      TString physShort = ((TObjString*)nameTokens->At(2))->String();

      // We'll only keep track of the e/s/r/p-tags for MC. Data will typically have fewer
      // name tokens, which is why we have to check how many there are here!
      TString sampleTags = "";
      if (nameTokens->GetLast() >= 5) sampleTags = ((TObjString*)nameTokens->At(5))->String();

      TString dsName = sh->getDSName(name, tag, "", isData, false, false);
      TString newDirName = TString::Format("user.unused.%s.%s", physShort.Data(), dsName.Data());

      TString condorFileNames = dataOrMC+"."+dsid+"."+physShort+"*"+sampleTags+"*root*";
      TString treeDir = baseDir+"/"+groupSet+"/"+tag+"/trees/"+newDirName;

      TString mkdirCmd = "mkdir " + treeDir;
      TString mvCmd  = "mv " + baseDir + "/" + subdirPrefix + "_" + "*/fetch/data-tree/" + condorFileNames + " " + treeDir;

      if(noSubmit){
        std::cout << mkdirCmd << std::endl;
        std::cout << mvCmd << std::endl;
      }
      else{
        int mkdirSuccess = TString( gSystem->GetFromPipe(mkdirCmd) ).Atoi();
        if(mkdirSuccess != 0){
          std::cout << "mkdir command failed with status " << mkdirSuccess << "! Command was: " << mkdirCmd << std::endl;
          abort();
        }
        int mvSuccess = TString( gSystem->GetFromPipe(mvCmd) ).Atoi();
        if(mvSuccess != 0){
          std::cout << "mv command failed with status " << mvSuccess << "! Command was: " << mvCmd << std::endl;
          abort();
        }
      }

    }
  }

  return 0;

}
