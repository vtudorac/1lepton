#ifndef TRUTHEVENT_XX_h
#define TRUTHEVENT_XX_h

#include <iostream>
#include <vector>

#include "TLorentzVector.h"
#include "TObject.h"

#include "TruthDecayContainer/Decay_boson.h"
#include "TruthDecayContainer/TruthEvent_VV.h"

typedef TLorentzVector tlv;


class TruthEvent_XX : public TruthEvent_VV {

 public:

  TruthEvent_XX();

  ~TruthEvent_XX();

  TruthEvent_XX(const TruthEvent_XX &rhs);

  virtual void set_evt_XX(const int &in_chi1_pdg, const int &in_chi2_pdg, 
			  const tlv &in_pN1A, const tlv &in_pN1B, 
			  const Decay_boson &in_B1, const Decay_boson &in_B2);
        
  virtual void finalize();

  virtual int getChiLabel(const int &pdg);
  virtual int getProdLabel(const int &pdg1,const int &pdg2);
  virtual int getBosonLabel(const Decay_boson &B);

  virtual int getSlepLabel(const int &pdg);
  virtual int getProdLabel_slepton(const int &pdg1,const int &pdg2);

  virtual void check_swap();

  virtual void swap12();

  virtual void print();

  virtual void clear();

  ClassDef(TruthEvent_XX,2);

  // tlv 
  tlv pchi1, pchi2; // EW gaugino
  tlv pN1A , pN1B; 

  // decay label
  int  prodLabel;
  int  prodLabel_slepton;
  int  decayLabel_chi1;
  int  decayLabel_chi2;

  // pdg
  int chi1_pdg, chi2_pdg;
  int slep1_pdg, slep2_pdg;


 private:  


};







#endif
