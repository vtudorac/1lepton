// System include(s):
#include <memory>
#include <cstdlib>
#include <string>
#include <iostream>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
#include <TStopwatch.h>
#include <TSystem.h>
#include "TObjArray.h"
#include "TObjString.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TStore.h"
#ifdef ROOTCORE
#include "xAODRootAccess/TEvent.h"
#else
#include "POOLRootAccess/TEvent.h"
#endif // ROOTCORE

// EDM include(s):
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODJet/JetContainer.h"

// Local include(s):
#include "TruthDecayContainer/DecayHandle.h"
#include "TruthDecayContainer/ProcClassifier.h"
#include "TruthDecayContainer/TruthDecayUtils.h"

int main( int argc, char* argv[] ) {

  // The application's name:
  const char* APP_NAME = argv[ 0 ];

  // Unload the command line arguments
  TString inFilePath="";
  
  for(int k=0; k<argc; ++k) {
    TString key = argv[k];
    if(key=="-inputFile")   inFilePath  = argv[k+1];
  }

  // Check if we received a file name:
  if ( inFilePath=="" ) {
    TruthDecayUtils::ERROR( APP_NAME, "No file name received!" );
    TruthDecayUtils::ERROR( APP_NAME, "  Usage: %s -inputFile [full path to the xAOD file]", APP_NAME );
    return 1;
  }

  // Open the input file:
  TruthDecayUtils::INFO( APP_NAME, "Opening file: %s", inFilePath.Data() );
  std::auto_ptr< TFile > ifile( TFile::Open( inFilePath, "READ" ) );
  ifile.get();

  // Create a TEvent object:
#ifdef ROOTCORE
  xAOD::TEvent event( xAOD::TEvent::kAthenaAccess );
#else
  POOL::TEvent event( POOL::TEvent::kAthenaAccess );
#endif

  CHECK( event.readFrom( ifile.get() ) );
  TruthDecayUtils::INFO( APP_NAME, "Number of events in the file: %i",
        static_cast< int >( event.getEntries() ) );

  xAOD::TStore store;
  
  // Get DSID
  const xAOD::EventInfo* eventInfo = 0;
  event.getEntry(0);
  CHECK( event.retrieve( eventInfo, "EventInfo") );

  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)){
    TruthDecayUtils::ERROR( APP_NAME, "This is a data file. Exit. ");
    return 0;
  }
  
  int dsid = eventInfo->mcChannelNumber();
  if(dsid<=0){
    TruthDecayUtils::ERROR( APP_NAME, "Could not retrieve DSID from the file. Exit. ");
    return 1;
  }

  // Create the tool(s) to test:
  DecayHandle *decayHandle = new DecayHandle();

  bool convertFromMeV=true; // display everything in GeV
  CHECK( decayHandle->init(dsid, convertFromMeV) );

  TruthEvent_Vjets *truthEvent_Vjets=0;
  TruthEvent_VV    *truthEvent_VV=0;
  TruthEvent_TT    *truthEvent_TT=0;
  TruthEvent_XX    *truthEvent_XX=0;
  TruthEvent_GG    *truthEvent_GG=0;

  std::cout << "  DSID: " << dsid << std::endl;

  if(ProcClassifier::IsWjets_Sherpa(dsid) || ProcClassifier::IsZjets_Sherpa(dsid) || ProcClassifier::IsGammaJets_Sherpa(dsid)){   // V+jets
    TruthDecayUtils::INFO(APP_NAME,"Going to test truthEvent_Vjets.");
    truthEvent_Vjets = new TruthEvent_Vjets();
  }
  if(ProcClassifier::IsVV(dsid) || ProcClassifier::IsWt(dsid) || ProcClassifier::IsZt(dsid)){   // VV (incl. t+W/Z)
    TruthDecayUtils::INFO(APP_NAME,"Going to test truthEvent_VV.");
    truthEvent_VV    = new TruthEvent_VV();
  }
  if(ProcClassifier::IsTT(dsid) || ProcClassifier::IsTTPlusX(dsid)){ // ttbar (+X)
    TruthDecayUtils::INFO(APP_NAME,"Going to test truthEvent_TT.");
    truthEvent_TT    = new TruthEvent_TT();
  }
  if(ProcClassifier::IsXX(dsid)){                                    // EWKino pair production (gauge boson mediated, direct decay only)
    TruthDecayUtils::INFO(APP_NAME,"Going to test truthEvent_XX.");
    truthEvent_XX    = new TruthEvent_XX();
  }
  if(ProcClassifier::IsGG(dsid)){                                    // Gluino pair production (only for gauge boson mediated decay, upto 1-step)
    TruthDecayUtils::INFO(APP_NAME,"Going to test truthEvent_GG.");
    truthEvent_GG    = new TruthEvent_GG();   
  }


  // ------------------------- Loop over the events ------------------------- //

  int nevt = 50;  // Just the first 50 events
  for ( int entry = 0; entry < nevt; ++entry ) {

    // Tell the object which entry to look at:
    event.getEntry( entry );

    // Retrieve containers
    const xAOD::TruthEventContainer*    truthEvents    = 0;
    const xAOD::TruthParticleContainer* truthParticles = 0;
    const xAOD::TruthParticleContainer* truthElectrons = 0;
    const xAOD::TruthParticleContainer* truthMuons     = 0;
    const xAOD::TruthParticleContainer* truthTaus      = 0;
    const xAOD::TruthParticleContainer* truthPhotons   = 0;
    const xAOD::TruthParticleContainer* truthNeutrinos = 0;
    const xAOD::TruthParticleContainer* truthBoson     = 0;
    const xAOD::TruthParticleContainer* truthTop       = 0;
    const xAOD::TruthParticleContainer* truthBSM       = 0;
    const xAOD::JetContainer*           truthJets      = 0;
    CHECK( event.retrieve( truthEvents,    "TruthEvents"    ));
    CHECK( event.retrieve( truthParticles, "TruthParticles" ));
    CHECK( event.retrieve( truthElectrons, "TruthElectrons" ));
    CHECK( event.retrieve( truthMuons,     "TruthMuons"     ));
    CHECK( event.retrieve( truthTaus,      "TruthTaus"      ));
    CHECK( event.retrieve( truthPhotons,   "TruthPhotons"   ));
    CHECK( event.retrieve( truthBoson,     "TruthBoson"     ));
    CHECK( event.retrieve( truthTop,       "TruthTop"       ));
    CHECK( event.retrieve( truthBSM,       "TruthBSM"       ));
    CHECK( event.retrieve( truthJets, "AntiKt4TruthDressedWZJets" ));

    decayHandle->loadContainers(truthParticles, truthElectrons, truthMuons, truthTaus, truthPhotons, truthNeutrinos, truthBoson, truthTop, truthBSM, truthJets);

    TruthDecayUtils::INFO(APP_NAME, "////////////////////// Event: %i //////////////////////////////", entry);

    CHECK( decayHandle->GetFakeLeptons() );

    // Fill if the sample is W+jets ...
    if     (truthEvent_Vjets){ 
      CHECK( decayHandle->GetDecayChain_Vjets(truthEvent_Vjets) );
      truthEvent_Vjets->print();
      
    // Fill if the sample is WW or t+W (t->bW){ ...
    }else if(truthEvent_VV){    
      CHECK( decayHandle->GetDecayChain_VV(truthEvent_VV) );
      truthEvent_VV->print();

    // Fill if the sample is ttbar (+X){ ...
    }else if(truthEvent_TT){    
      CHECK( decayHandle->GetDecayChain_TT(truthEvent_TT) );
      truthEvent_TT->print();        

    // Fill if the sample is EW gaugino pair prod.
    }else if(truthEvent_XX){    
      CHECK( decayHandle->GetDecayChain_XX(truthEvent_XX) );
      truthEvent_XX->print();            

    // Fill if the sample is gluino pair prod. 
    }else if(truthEvent_GG){    
      CHECK( decayHandle->GetDecayChain_GG(truthEvent_GG) );
      truthEvent_GG->print();            
    }
    

  }

  return 0;
}
