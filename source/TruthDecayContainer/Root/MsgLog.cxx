#include "TruthDecayContainer/MsgLog.h"
#include "PathResolver/PathResolver.h"

// ROOT includes
#include "TSystem.h"

static std::ofstream* m_warning_log = 0; 
static std::ofstream* m_error_log = 0;

// Keeping track of number of warnings/errors
static std::map<TString,int> m_printLog;

// Max number of errors
static int m_maxPrint = 500;

MsgLog::MsgLog()
{


}
// --------------------------------------------------------- //
void MsgLog::INFO(TString className,const char *va_fmt, ...)
{

  va_list args;
  va_start (args, va_fmt);
  TString infoMsg = format(va_fmt,args);
  va_end (args);

  TString line = TString::Format("%s",formatOutput(className,infoMsg,"INFO").Data()  );

  // Print
  std::cout << line << std::endl;

}
// --------------------------------------------------------- //
void MsgLog::WARNING(TString className,const char *va_fmt, ...)
{

  // Clean up previous log files
  // Make a new one
  if( !m_warning_log ){
    TString path = PathResolverFindDataFile("SusySkimMaker/warningMsg.txt");
    gSystem->Exec( TString::Format("rm -f %s",path.Data()) );
    m_warning_log = new std::ofstream( path.Data(), std::ios::app | std::ios::out );
  }

  va_list args;
  va_start (args, va_fmt);
  TString warningMsg = format(va_fmt,args);
  va_end (args);

  TString line = TString::Format("%s",formatOutput(className,warningMsg,"WARNING").Data()  );

  // Print
  if( doPrint(line) ){
    std::cout << line << std::endl;
    // Append
    *m_warning_log << line.Data() << std::endl;
  }



}
// --------------------------------------------------------- //
void MsgLog::ERROR(TString className,const char *va_fmt, ...)
{

  // Clean up previous log files
  // Make a new one
  if( !m_error_log ){
    TString path = PathResolverFindDataFile("SusySkimMaker/errorMsg.txt");
    gSystem->Exec( TString::Format("rm -f %s",path.Data()) );
    m_error_log = new std::ofstream( path.Data(), std::ios::app | std::ios::out );
  }

  va_list args;
  va_start (args, va_fmt);
  TString errorMsg = format(va_fmt,args);
  va_end (args);

  TString line = TString::Format("%s",formatOutput(className,errorMsg,"ERROR").Data()  );

  // Print
  if( doPrint(line) ){
    std::cout << line << std::endl;
    // Append
    *m_error_log << line.Data() << std::endl;
  }


}
// --------------------------------------------------------- //
TString MsgLog::formatOutput(TString className,TString msg, TString msgStream)
{

  Int_t MAX_CLASS_SIZE = 40;

  // Fixed length
  if( className.Length()>MAX_CLASS_SIZE ){
    className = className.Replace(MAX_CLASS_SIZE-3,className.Length(),"...",3);
  }

  TString line = TString::Format("%-43s%-20s%-10s",className.Data(),msgStream.Data(),msg.Data() );
  return line;

}
// --------------------------------------------------------- //
TString MsgLog::format(const char* va_fmt, va_list args)
{

  char buffer[512];
  vsnprintf (buffer,512,va_fmt,args);

  TString formatted_string = TString(buffer);
  return formatted_string;

}
// --------------------------------------------------------- //
bool MsgLog::doPrint(TString msg)
{

  auto it = m_printLog.find(msg);
  if( it==m_printLog.end() ){
    m_printLog.insert( std::pair<TString,int>(msg,1) );
    return true;
  }
  else{
    if( it->second>=m_maxPrint ){
      return false;
    }
    //
    it->second++;
    return true;
  }
  




}
