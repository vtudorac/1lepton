#include "TruthDecayContainer/TruthEvent_Vjets.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/TruthDecayUtils.h"


typedef TLorentzVector tlv;

///////////////////////////////////////////////////
TruthEvent_Vjets::TruthEvent_Vjets() : 
  mV(0.),
  MET(0.),
  hasCharm(false),
  hasBottom(false),
  decayLabel(-1)
{    
  clear();  
}

///////////////////////////////////////////////////
TruthEvent_Vjets::TruthEvent_Vjets(const TruthEvent_Vjets &rhs) : 
  TObject(rhs),
  V(rhs.V),
  addPartons(rhs.addPartons),
  addPartons_pdg(rhs.addPartons_pdg),
  truthJets(rhs.truthJets),
  truthJets_label(rhs.truthJets_label),
  mV(rhs.mV),
  MET(rhs.MET),
  hasCharm(rhs.hasCharm),
  hasBottom(rhs.hasBottom),
  decayLabel(rhs.decayLabel)
{}                                                       

///////////////////////////////////////////////////
TruthEvent_Vjets::~TruthEvent_Vjets(){}

///////////////////////////////////////////////////
void TruthEvent_Vjets::set_evt(const Decay_boson &in_V, 
			       const std::vector<tlv> &in_addPartons,
			       const std::vector<int> &in_addPartons_pdg,
			       const std::vector<tlv> &in_truthJets,
			       const std::vector<int> &in_truthJets_label){
  V              = in_V ;
  addPartons     = in_addPartons;
  addPartons_pdg = in_addPartons_pdg;

  truthJets       = in_truthJets;
  truthJets_label = in_truthJets_label;

}
///////////////////////////////////////////////////
void TruthEvent_Vjets::finalize(){

  // input : W children's tlv, pdg
  //         (tauchild's tlv, pdg, decayLabel)

  V.finalize();

  // ++++++++ derived variables ++++++++ //
  mV  = V.P4.M();
  MET = V.pMis.Pt();

  // 
  hasCharm=false; 
  hasBottom=false;
  for(int ip=0; ip<(int)addPartons_pdg.size(); ++ip){
    if(std::abs(addPartons_pdg[ip])==4) hasCharm=true;
    if(std::abs(addPartons_pdg[ip])==5) hasBottom=true;
  }

  decayLabel     = V.decayLabel;

  return;
}
///////////////////////////////////////////////////
void TruthEvent_Vjets::print(){


  std::cout << "  TruthEvent_Vjets::print() --------------------------------------" << std::endl;
  V.print();
  
  std::cout << "  ----- Additional partons" << std::endl;
  for(int ip=0; ip<(int)addPartons.size(); ++ip){
    std::cout << ip <<":  pdgId: " <<  addPartons_pdg[ip] << std::endl;
    TruthDecayUtils::print4mom(addPartons[ip],  "");
  }
  std::cout << "    hasCharm: " << hasCharm << std::endl;
  std::cout << "    hasBottom: " << hasBottom << std::endl;

  std::cout << "  --------------------------------------------------------" << std::endl;


  return;
}
///////////////////////////////////////////////////
void TruthEvent_Vjets::clear(){

  // tlv
  V.clear();
  addPartons.clear();
  addPartons_pdg.clear();
  truthJets.clear();
  truthJets_label.clear();

  // derived variables
  mV=0;  MET=0;

  // addtional parton flavor
  hasCharm=false;
  hasBottom=false;

  // Decay label
  decayLabel=-1;
  
  return;
}
