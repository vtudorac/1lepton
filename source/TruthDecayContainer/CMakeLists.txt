# $Id: CMakeLists.txt 797759 2017-02-18 01:24:37Z zmarshal $
################################################################################
# Package: TruthDecayContainer
################################################################################

 
# Declare the package name:
atlas_subdir( TruthDecayContainer )

# Extra dependencies based on the build environment:
set( extra_deps )
set( extra_libs )

# Declare the package's dependencies:
#atlas_depends_on_subdirs(
#   PUBLIC
#   Control/AthToolSupport/AsgTools
#   Event/xAOD/xAODBase
#   Event/xAOD/xAODMetaData
#   PhysicsAnalysis/AnalysisCommon/PATInterfaces
#   PhysicsAnalysis/AnalysisCommon/CPAnalysisExamples
#   ${extra_deps} 
#)

# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist Physics RIO Minuit )
find_package( GTest )

#if( XAOD_STANDALONE )
   atlas_add_root_dictionary( TruthDecayContainerLib TruthDecayContainerCintDict
      ROOT_HEADERS TruthDecayContainer/*.h Root/LinkDef.h
      EXTERNAL_PACKAGES ROOT )
#endif()


# Libraries in the package:
atlas_add_library( TruthDecayContainerLib STATIC
   TruthDecayContainer/*.h Root/*.cxx ${TruthDecayContainerCintDict}
   PUBLIC_HEADERS TruthDecayContainer
   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES AsgTools xAODCore xAODEventInfo xAODJet xAODTruth xAODMetaData TauAnalysisToolsLib
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES}
   PathResolver TauAnalysisToolsLib xAODBase ${extra_libs} )

#atlas_add_dictionary( TruthDecayContainerDict
#   TruthDecayContainer/TruthDecayContainerDict.h
#   TruthDecayContainer/selection.xml
#   LINK_LIBRARIES TruthDecayContainerLib )


atlas_add_executable( TruthDecayContainerTester
      util/TruthDecayContainerTester.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} PATInterfaces
      xAODRootAccess AsgTools TruthDecayContainerLib RootCoreUtils )

atlas_add_executable( EWKinoPolReweighting
      util/EWKinoPolReweighting.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools
      xAODRootAccess AsgTools TruthDecayContainerLib RootCoreUtils )


# Install files from the package:
#atlas_install_python_modules( python/*.py )
#atlas_install_joboptions( share/*.py )
#atlas_install_data( data/* )









