#ifndef xAODNtupleMaker_xAODEvtLooper_H
#define xAODNtupleMaker_xAODEvtLooper_H

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include <EventLoop/Algorithm.h>

// ROOT include(s)
#include "TFile.h"
#include "TH1.h"
#include "TDirectory.h"

// Forward declarations
class ObjectTools;
class TreeMaker;
class ConfigMgr;
class Objects;
class BaseUser;

// External packages that use this driver

#if __has_include("SusySkimModelling/SusySkimModelling_Interface.h")
  #include "SusySkimModelling/SusySkimModelling_Interface.h"
#endif

#if __has_include("SusySkimValidation/SusySkimValidation_Interface.h")
  #include "SusySkimValidation/SusySkimValidation_Interface.h"
#endif

#if __has_include("SusySkim1LInclusive/SusySkim1LInclusive_Interface.h")
  #include "SusySkim1LInclusive/SusySkim1LInclusive_Interface.h"
#endif

#if __has_include("SusySkimHiggsino/SusySkimHiggsino_Interface.h")
  #include "SusySkimHiggsino/SusySkimHiggsino_Interface.h"
#endif

#if __has_include("SusySkimLongLived/SusySkimLongLived_Interface.h")
  #include "SusySkimLongLived/SusySkimLongLived_Interface.h"
#endif

#if __has_include("SusySkimEWK3L/SusySkimEWK3L_Interface.h")
  #include "SusySkimEWK3L/SusySkimEWK3L_Interface.h"
#endif

#if __has_include("SusySkim2LJetsLegacy/SusySkim2LJetsLegacy_Interface.h")
  #include "SusySkim2LJetsLegacy/SusySkim2LJetsLegacy_Interface.h"
#endif

#if __has_include("SusySkimHaa/SusySkimHaa_Interface.h")
  #include "SusySkimHaa/SusySkimHaa_Interface.h"
#endif

#if __has_include("SusySkimPhotons/SusySkimPhotons_Interface.h")
  #include "SusySkimPhotons/SusySkimPhotons_Interface.h"
#endif

#if __has_include("SusySkimPLT/SusySkimPLT_Interface.h")
  #include "SusySkimPLT/SusySkimPLT_Interface.h"
#endif

#if __has_include("SusySkimEWKHad/SusySkimEWKHad_Interface.h")
  #include "SusySkimEWKHad/SusySkimEWKHad_Interface.h"
#endif

#if __has_include("SusySkimEWK1Step/SusySkimEWK1Step_Interface.h")
  #include "SusySkimEWK1Step/SusySkimEWK1Step_Interface.h"
#endif

namespace ST
{
  class SUSYObjDef_xAOD;
}

class xAODEvtLooper : public EL::Algorithm
{

 // Event Loop 
 public:
  xAOD::TEvent* m_event;    //!

  /// Constructor
  xAODEvtLooper ();

  // Event loop functions
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize(){ return EL::StatusCode::SUCCESS; }
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute () { return EL::StatusCode::SUCCESS; }
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize () { return EL::StatusCode::SUCCESS; }

  ///
  /// Truth only analysis
  ///
  EL::StatusCode executeTruthOnly();


  /// Methods to set flags
  void isData(bool isData){m_isData=isData;}
  void isAf2(bool isAf2){m_isAf2=isAf2;}
  void isTruthOnly(bool isTruthOnly){m_isTruthOnly=isTruthOnly;}
  void writeSkims(int writeSkims){m_writeSkims=writeSkims;}
  void writeTrees(int writeTrees){m_writeTrees=writeTrees;}
  void writeMetaData(int writeMetaData){m_writeMetaData=writeMetaData;}
  void setNumEvents(float nEvents){m_nEvents=nEvents;}
  void setDAODKernelName(TString DAODKernelName){m_derivationName=DAODKernelName;}
  void setNoCutBookkeepers(bool noCutBookkeepers){m_noCutBookkeepers=noCutBookkeepers;}
  void setDisableCuts(bool disableCuts){m_disableCuts=disableCuts;}
  void setSystematicSet(TString sysSet){m_sysSetName=sysSet;}
  void setSampleName(TString sampleName){m_sampleName=sampleName;}
  void setDeepConfig(TString deepConfig){m_deepConfig=deepConfig;}
  void setSelectorName(std::string selectorName){m_selectorName=selectorName;}
  void setMaxEvents(int MaxEvents){m_nProcEvents = MaxEvents;}

  ///
  /// Loop Helpers
  ///

  ///
  ///  Print out the time lapse and the ETA to complete the loop (per file)
  ///
  void reportLapse(const int &ievt, const int &nevt, const clock_t &clock_start);

  ///
  ///  Check the existence of jet/truth particle container in the file
  ///
  bool checkJetContainer(std::string containerName);
  bool checkTruthParticleContainer(std::string containerName);
  bool checkTruthEventContainer(std::string containerName);
  bool checkTruthVertexContainer(std::string containerName);
  /////////////////////////////////////////////////////////////////

  ///
  /// Retrieve current mem usage of the machine (in MB)
  ///
  void getMemUsage(double& vm_usage, double& resident_set);  

 protected:
 
  ///
  /// Global ConfigMgr.
  ///  
  ConfigMgr*               m_configMgr;        //!

  ///
  /// Restore directory after tools are initialized 
  ///	
  TDirectory*              m_dir;              //!

  ///
  /// Global TStore object. TODO: Check using store from EventLoop
  ///
  xAOD::TStore*            m_store;            //!

  ///
  /// BaseUser class, used to interface a given analysis selector to this class
  ///
  BaseUser*                m_baseUser;         //!

  ///
  /// Running over data?
  ///
  int m_isData;

  ///
  /// Fast simulation sample?
  ///
  int m_isAf2;

  ///
  /// Write out truth information only? Overloading all Object contains to have truth information.
  ///
  int m_isTruthOnly;

  ///
  /// Write out skims. See -h for details.
  ///
  int m_writeSkims;

  ///
  /// Write out trees. See -h for details.
  ///
  int m_writeTrees;

  ///
  /// Write out metaData. 0==no meta data, 1==skim only, 2==tree only, 3==both skim and tree files
  ///
  int m_writeMetaData;

  /// 
  /// Manually set number of events to normalize the MC to.
  ///
  float m_nEvents;

  /// 
  /// DAOD kernel in the CutBookkeepers to use. One in the deep config will be overwritten.
  ///
  TString m_derivationName;

  ///
  /// if CutBookkeepers are not available, use nEvents from command line and sum up sum-of-weights in the event loop
  ///
  bool m_noCutBookkeepers;

  ///                                                  
  /// Skip all the cuts defined in the user selector?
  ///                                                  
  bool m_disableCuts;

  ///
  /// Systematic set name
  ///
  TString m_sysSetName;

  ///
  /// Name of a analysis selector you want to use.
  ///
  std::string m_selectorName;

  ///
  /// Sample name
  ///
  TString m_sampleName;

  ///
  /// Deep config file
  ///
  TString m_deepConfig;

  ///
  /// True if file is a derivation (will be determined from metadata)
  ///
  bool m_isDerivation;

  ///
  /// SumOfWeights and nEvents for the currently processed file from metadata for all Events and Derivation
  /// Note: sumOfWeightsPerEvent only works to determine the normalization if no events are skipped (by a cut)
  ///
  float m_fileTotalSOW;
  float m_fileTotalNEvents;
  float m_fileDerivationSOW;
  float m_fileDerivationNEvents;
  float m_sumOfWeightsPerEvent;

  ///
  /// Number of events to loop / event counter / clock to calculate the ETA
  ///
  int     m_nProcEvents;
  int     m_ievent;
  clock_t m_clock_fileStart;

  ///
  /// flag to store the option if additional MET values should be calculated, flagging muons as invisible particles
  ///
  bool m_add_MET_with_muons_invisible;

  ///
  /// flag to store the option if additional MET values should be calculated, flagging leptons as invisible particles
  ///
  bool m_add_MET_with_leptons_invisible;
  

  ///
  /// flags to store the option if additional MET WPs (Loose, Tighter, Tenacious) should be calculated
  ///
  bool m_add_loose_MET_WP;
  bool m_add_tighter_MET_WP;
  bool m_add_tenacious_MET_WP;

  ///
  /// Do trigger emulation for higgsino triggers?
  ///
  bool m_emulateAuxTriggers;

  ///
  /// Turns on and off saving of taus; default off; controled by CentealDB
  ///
  bool m_saveTaus;

 public:

  ///
  /// truth jet container name used for retrieval
  ///
  std::string m_truthJetContainerName;

  ///
  /// flag to process/store photons (default: true)
  ///
  bool m_doPhoton;

  ///
  /// flag to process/store fat jets (default: faslse)
  ///
  bool m_doFatjet;
  std::string m_fatjetContainerName;
  std::string m_truthFatjetContainerName;

  ///
  /// flag to process/store track jets (default: faslse)
  ///
  bool m_doTrackjet;
  std::string m_trackjetContainerName;

  ///
  /// Does the file contain truth particle containers?
  ///
  bool m_containTruthParticles;
  bool m_containTruthNeutrinos;
  bool m_containTruthBosonWDP;
  bool m_containTruthTopWDP;
  bool m_containTruthBSMWDP;
  bool m_containTruthEvents;
  bool m_containTruthTaus;
  bool m_containBornLeptons;
  bool m_containTruthVertices;

 public:

  // this is needed to distribute the algorithm to the workers
  ClassDef(xAODEvtLooper, 1);

};

#endif
